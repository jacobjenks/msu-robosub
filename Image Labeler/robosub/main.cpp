#include "opencv2\highgui\highgui.hpp"
#include "tinyxml2.h"
#include <boost/filesystem.hpp>
#include <iostream>


using namespace cv;
using namespace std;
namespace fs = boost::filesystem;

struct rect{
	Point2f p1;
	Point2f p2;
	string label;
};

bool lDown, rDown;
Point2f rect1, rect2;
vector<rect> rects;
vector<string> labels;
const int fontWidth = 10;
const int fontHeight = 12;
const int fontPadding = 5;
const Point2f labelOffset(5, -20);
vector<string> allowedExtensions;
vector<fs::path> images;
int currentFile, currentLabel;
tinyxml2::XMLDocument doc;
Mat image;
bool running = true;

bool pointInRect(float minX, float maxX, float minY, float maxY, float x, float y){
	if (x > minX && x < maxX && y > minY && y < maxY)
		return true;
	return false;
}

void on_mouse(int event, int x, int y, int flags, void*){
	switch (event){
		case EVENT_LBUTTONDOWN:
			lDown = true;
			rect1 = Point2f(x, y);
			break;
		case EVENT_LBUTTONUP://create rect
			lDown = false;
			rect2 = Point2f(x, y);
			if (rect1 != Point2f(-1, -1)){
				rect r;
				r.p1 = rect1;
				r.p2 = rect2;
				r.label = labels[currentLabel];
				rects.push_back(r);
				rect1 = rect2 = Point2f(-1, -1);
			}
			break;
		case EVENT_RBUTTONDOWN:
			rDown = true;
			break;
		case EVENT_RBUTTONUP://Delete rects in right click
			rDown = false;
			float minX, minY, maxX, maxY;
			for (int i = 0; i < rects.size(); i++){
				if (rects[i].p1.x > rects[i].p2.x){
					minX = rects[i].p2.x;
					maxX = rects[i].p1.x;
				}
				else{
					minX = rects[i].p1.x;
					maxX = rects[i].p2.x;
				}
				if (rects[i].p1.y > rects[i].p2.y){
					minY = rects[i].p2.y;
					maxY = rects[i].p1.y;
				}
				else{
					minY = rects[i].p1.y;
					maxY = rects[i].p2.y;
				}
				if (pointInRect(minX, maxX, minY, maxY, x, y)){
					rects.erase(rects.begin() + i);
					i--;
				}
			}
			break;
		case EVENT_MOUSEMOVE:
			if (lDown)
				rect2 = Point2f(x, y);
			break;
	}
}

void shutdown(){
	destroyAllWindows();
}


void loadImage(){
	rects.clear();
	image = imread(images[currentFile].string(), CV_LOAD_IMAGE_COLOR);

	//Load annotations
	fs::path anno(images[currentFile].parent_path().parent_path().string() + "/Annotations/" + images[currentFile].stem().string() + ".xml");
	if (fs::exists(anno) && fs::is_regular_file(anno)){
		doc.LoadFile(anno.string().c_str());
		//get objects
		for (tinyxml2::XMLElement *current = doc.FirstChildElement("annotation")->FirstChildElement("object"); current != NULL; current = current->NextSiblingElement("object")){
			Point2f p1(stof(current->FirstChildElement("bndbox")->FirstChildElement("xmin")->GetText()), stof(current->FirstChildElement("bndbox")->FirstChildElement("ymin")->GetText()));
			Point2f p2(stof(current->FirstChildElement("bndbox")->FirstChildElement("xmax")->GetText()), stof(current->FirstChildElement("bndbox")->FirstChildElement("ymax")->GetText()));

			rect r;
			r.label = current->FirstChildElement("name")->GetText();
			r.p1 = p1;
			r.p2 = p2;
			rects.push_back(r);
		}
	}
}

void handleConsole(){
	String s;
	//cin >> s;
	s = "C:/Users/Jake/Desktop/VOCtrainval_06-Nov-2007/VOCdevkit/VOC2007/JPEGImages";
	fs::path dir(s);
	fs::directory_iterator end_iter;


	//Get images
	vector<fs::path> files = vector<fs::path>();

	if (fs::exists(dir) && fs::is_directory(dir)){
		for (fs::directory_iterator dir_iter(dir); dir_iter != end_iter; ++dir_iter){
			if (fs::is_regular_file(dir_iter->status()) && std::find(allowedExtensions.begin(), allowedExtensions.end(), fs::extension(dir_iter->path())) != allowedExtensions.end())
			{
				files.push_back(*dir_iter);
			}
		}
	}
	else{
		cout << "Directory is not valid!" << endl;
	}

	cout << files.size() << " image files found." << endl;

	//Check for annotation directory
	fs::path anno(dir.parent_path().string() + "/Annotations");
	if (fs::exists(anno) && fs::is_directory(anno)){
		cout << "Annotation directory found." << endl;
	}
	else{
		try{
			cout << "No annotation directory found. Creating..." << endl;
			fs::create_directory(anno);
		}
		catch (boost::filesystem::filesystem_error e){
			cout << "Failed to create directory. Please manually create a folder called 'Annotations' one directory level higher than the one you specified." << endl;
		}
	}

	images = files;
	currentFile = 0;
	loadImage();
}


Mat drawRect(Mat image, Point2f p1, Point2f p2, string label, Scalar color){
	rectangle(image, p1, p2, color);
	if (label != ""){
		rectangle(image, Point2f(p1 + labelOffset), Point2f(p1 + labelOffset + Point2f((fontWidth * label.size()) + fontPadding, fontHeight + fontPadding)), color, CV_FILLED);//Draw label box
		putText(image, label, p1 + labelOffset + Point2f(fontPadding / 2, fontHeight), CV_FONT_HERSHEY_PLAIN, 1, Scalar(255, 255, 255), 1);//Draw label text
	}
	return image;
}

Mat drawLabels(){
	Mat displayImage = image.clone();
	if (rect1 != Point2f(-1, -1) && rect2 != Point2f(-1, -1))
		displayImage = drawRect(displayImage, rect1, rect2, "", Scalar(0, 255, 0));
	for (int i = 0; i < rects.size(); i++){
		displayImage = drawRect(displayImage, rects[i].p1, rects[i].p2, rects[i].label, Scalar(0, 0, 255));
	}
	return displayImage;
}

void init(){
	allowedExtensions = vector<string>();
	allowedExtensions.push_back(".png");
	allowedExtensions.push_back(".jpg");
	allowedExtensions.push_back(".jpeg");
	allowedExtensions.push_back(".gif");

	currentFile = -1;

	images = vector<fs::path>();

	lDown = rDown = false;
	rect1 = rect2 = Point2f(-1, -1);
	rects = vector<rect>();
	labels = vector<string>();

	labels.push_back("Buoy");
	labels.push_back("Gate");
	labels.push_back("Torpedo");
	currentLabel = 0;


	cout << "Drag left click to create a label. Right click on a label to delete it. Press x to delete all labels." << std::endl;
	cout << "Press < > to change images" << std::endl;
	cout << "Press O to choose a directory to load." << std::endl;
	cout << "Type in the directory to label:" << std::endl;
	//create output windows
	namedWindow("Image Labeler", WINDOW_AUTOSIZE | CV_GUI_EXPANDED);

	//create mouse callbacks
	setMouseCallback("Image Labeler", on_mouse, 0);

	string console = "";

	image = Mat(600, 600, CV_8UC3, Scalar(1, 1, 1));
}

void handleInput(char w){
	switch (w){
		case 44:
			if (currentFile > 0){
				currentFile--;
				loadImage();
			}
			break;
		case 46:
			if (currentFile < images.size() - 1){
				currentFile++;
				loadImage();
			}
			break;
		case 111:
			handleConsole();
			break;
	}
}

int main(int argc, char** argv)
{
	init();
	handleConsole();

	while (running){

		imshow("Image Labeler", drawLabels());
		int w = waitKey(1);
		if (w != -1){
			handleInput(w);
		}
	}
	
	shutdown();
	return 0;
}
