int pressureSensorPin = A0;    // select the input pin for the potentiometer
int firstSensor = 0;  // variable to store the value coming from the sensor

int markerDroperPinL= 22;
int markerDroperPinR = 24;
int torpedoPinL = 26;
int torpedoPinR = 28;

int airValveDelay_ms = 100;

int inByte = 0; 


void setup()
{
  // start serial port at 9600 bps and wait for port to open:
  Serial.begin(9600);
  pinMode(markerDroperPinL, OUTPUT);
  pinMode(markerDroperPinR, OUTPUT);
  pinMode(torpedoPinL, OUTPUT);
  pinMode(torpedoPinR, OUTPUT);
  //establishContact();  // send a byte to establish contact until receiver responds 
}

void loop()
{
  // if we get a valid byte, read analog ins:
  if (Serial.available() > 0) {
    // get incoming byte:
    inByte = Serial.read();
    
   if (inByte == '1')
   {
      // read Pressure Sensor analog input:
      firstSensor = analogRead(pressureSensorPin);
      // send sensor values:
      Serial.println(firstSensor);
   }
    else if (inByte == 'a')
   {  // marker dropper 
     digitalWrite(markerDroperPinL, 1);
     delay(airValveDelay_ms);
     digitalWrite(markerDroperPinL, 0);  
   } 
    else if (inByte == 'b')
   {  // marker dropper 
     digitalWrite(markerDroperPinR, 1);
     delay(airValveDelay_ms);
     digitalWrite(markerDroperPinR, 0);   
   } 
    else if (inByte == 'c')
   {  // Torpedo
    digitalWrite(torpedoPinL, 1); 
    delay(airValveDelay_ms);
    digitalWrite(torpedoPinL, 0);   
   } 
    else if (inByte == 'd')
   {  // Torpedo
    digitalWrite(torpedoPinR, 1);
    delay(airValveDelay_ms);
    digitalWrite(torpedoPinR, 0);   
   } 
   else 
   {
    // no op
   }

         
             
    Serial.flush();
  }
    
  delay(1);
}

void establishContact() {
  while (Serial.available() <= 0) {
    Serial.println("0,0,0");   // send an initial string
    delay(300);
  }
}
