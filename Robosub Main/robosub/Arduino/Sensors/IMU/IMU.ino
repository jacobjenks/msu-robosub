//This has some good info: https://learn.sparkfun.com/tutorials/serial-communication
#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11); // RX, TX
char x;
byte b1, b2, b3;
String stringData;
String byteData;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // set the data rate for the SoftwareSerial port
  mySerial.begin(115200);
  Serial.write("Initialized\n");
}

void loop() { // run over and over
  if (Serial.available()) {
    Serial.write("Sending message...\n");
    while(Serial.available()){
      x = Serial.read();
      //mySerial.write(x);
      //Serial.write(x);
    }
    //NMEA text interface
    //Other messages:
    //  $PSPA,MR\r\n
    //mySerial.write("$xxHDM\r\n");
    
    //Legacy binary interface
    
    b1 = 0xA4;
    b2 = 0x01;
    b3 = 0xA0;
    mySerial.write(b1);
    mySerial.write(b2);
    mySerial.write(b3);
    
  }
  if (mySerial.available()) {
    Serial.write("Received message...\n");
    while(mySerial.available()){
      Serial.print(mySerial.read());
    }
    
    //This is for printing bytes as decimal values
    /*
        while(mySerial.available()){
      Serial.print(mySerial.read());
      Serial.print("\n");
    }
    */
    
    Serial.print("\n");
  }
}
