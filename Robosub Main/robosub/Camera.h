#pragma once

#include "Updateable.h"
#include <string>
#include <memory>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "MessageDisplay.h"
#include "KeyboardListener.h"

using namespace std;
using namespace cv;

class Camera : public Updateable{
private:
	string name;
	VideoCapture video;
	Mat lastFrame;
	bool valid, paused;
	unique_ptr<KeyboardListener> k;
public:
	Camera(const string name, const int camNumber);
	void update();
	Mat getLastFrame();
};