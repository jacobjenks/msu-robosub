#include "Navigation.h"

Navigation::Navigation(){

	defaultPower = 100;

	valid = true;

	frontStrafe = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Normal, "00071571"));
	backStrafe = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Reverse, "00071573"));
	left = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Normal, "00071568"));
	right = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Reverse, "00071575"));
	backDepth = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Reverse, "00071566"));
	frontDepth = shared_ptr<JrkMotor>(new Jrk::JrkMotor(MotorOrientation::M_Normal, "00071580"));

	if (frontStrafe->controllerNull()){
		MessageDisplay::add(MessageType::message, "Front strafe motor failed to initialize");
		valid = false;
	}
	if (backStrafe->controllerNull()){
		MessageDisplay::add(MessageType::message, "Back strafe motor failed to initialize");
		valid = false;
	}
	if (left->controllerNull()){
		MessageDisplay::add(MessageType::message, "Left motor failed to initialize");
		valid = false;
	}
	if (right->controllerNull()){
		MessageDisplay::add(MessageType::message, "Right motor failed to initialize");
		valid = false;
	}
	if (backDepth->controllerNull()){
		MessageDisplay::add(MessageType::message, "Back depth motor failed to initialize");
		valid = false;
	}
	if (frontDepth->controllerNull()){
		MessageDisplay::add(MessageType::message, "Front depth motor failed to initialize");
		valid = false;
	}
}

Navigation::~Navigation(){
	MessageDisplay::add(MessageType::message, "Shutting down motors");
	setAll(MotorDirection::Stop);
}

vector<string> Navigation::getMotorStatus(){
	vector<string> status;
	status.push_back("Left: " + left->getStatus());
	status.push_back("Right: " + right->getStatus());
	status.push_back("FStrafe: " + frontStrafe->getStatus());
	status.push_back("BStrafe: " + backStrafe->getStatus());
	status.push_back("FDepth: " + frontDepth->getStatus());
	status.push_back("BDepth: " + backDepth->getStatus());
	return status;
}

bool Navigation::checkPower(float power){
	if (power < 0 || power > 1)
		return false;
	return true;
}

bool Navigation::setMotor(shared_ptr<JrkMotor> motor, MotorDirection d){
	if (!valid)
		return false;
	switch (d){
		case MotorDirection::Forward:
			return motor->motorForward(defaultPower);
		case MotorDirection::Backward:
			return motor->motorBackward(defaultPower);
		case MotorDirection::Stop:
			return motor->motorStop();
	}
	return false;
}

bool Navigation::setMotor(shared_ptr<JrkMotor> motor, MotorDirection d, float power){
	if (!checkPower(power)){
		MessageDisplay::add(MessageType::message, "Invalid motor power value.");
		return false;
	}
	power *= 100;
	if (!valid)
		return false;
	switch (d){
		case MotorDirection::Forward:
			return motor->motorForward(power);
		case MotorDirection::Backward:
			return motor->motorBackward(power);
		case MotorDirection::Stop:
			return motor->motorStop();
	}
	return false;
}

bool Navigation::setFrontStrafe(MotorDirection d){
	return setMotor(frontStrafe, d);
}

bool Navigation::setBackStrafe(MotorDirection d){
	return setMotor(backStrafe, d);
}

bool Navigation::setLeft(MotorDirection d){
	return setMotor(left, d);
}

bool Navigation::setRight(MotorDirection d){
	return setMotor(right, d);
}

bool Navigation::setFrontDepth(MotorDirection d){
	return setMotor(frontDepth, d);
}

bool Navigation::setBackDepth(MotorDirection d){
	return setMotor(backDepth, d);
}

bool Navigation::setAll(MotorDirection d){
	bool result = true;
	result &= setMotor(frontStrafe, d);
	result &= setMotor(backStrafe, d);
	result &= setMotor(left, d);
	result &= setMotor(right, d);
	result &= setMotor(frontDepth, d);
	result &= setMotor(backDepth, d);
	return result;
}

//With power
bool Navigation::setFrontStrafe(MotorDirection d, float power){
	return setMotor(frontStrafe, d, power);
}

bool Navigation::setBackStrafe(MotorDirection d, float power){
	return setMotor(backStrafe, d, power);
}

bool Navigation::setLeft(MotorDirection d, float power){
	return setMotor(left, d, power);
}

bool Navigation::setRight(MotorDirection d, float power){
	return setMotor(right, d, power);
}

bool Navigation::setFrontDepth(MotorDirection d, float power){
	return setMotor(frontDepth, d, power);
}

bool Navigation::setBackDepth(MotorDirection d, float power){
	return setMotor(backDepth, d, power);
}

bool Navigation::setAll(MotorDirection d, float power){
	bool result = true;
	result &= setMotor(frontStrafe, d, power);
	result &= setMotor(backStrafe, d, power);
	result &= setMotor(left, d, power);
	result &= setMotor(right, d, power);
	result &= setMotor(frontDepth, d, power);
	result &= setMotor(backDepth, d, power);
	return result;
}