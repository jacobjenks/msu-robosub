#pragma once

#include "phidget21.h"
#include "MessageDisplay.h"

/*
* File:   JrkController.hpp
* Author: arbiter34
*
* Created on February 6, 2015, 2:10 PM
*/

#ifndef IMU_HPP
#define	IMU_HPP

#include <sstream>
#include <atomic>
#include <mutex>
#include "RUtility.h"

namespace Imu
{
	/// <summary>
	/// The different boards.
	/// </summary>

	static std::stringstream os;

	class ImuController
	{
		private:
            CPhidgetSpatialHandle phid;
			CPhidgetSpatial_SpatialEventData* spatialData;
			double roll, pitch, yaw;//Range from 0 to 360
			double lastMag[3];
			double lastGyro[3];
			double lastAccel[3];
		protected:
		public:
			ImuController();
			static std::mutex mutex;
			CPhidgetSpatial_SpatialEventData* data;
			double* getMagneticReading();
			double* getGyroData();
			double* getAccelData();
			double getRoll(){return roll;};
			double getPitch(){return pitch;};
			double getYaw(){return yaw;};
			void update();
			void computeAngles();

	};

}




#endif	/* IMU_HPP */

