#pragma once

/*
	Anything that should be updated in the main loop should implement this interface.
*/
class Updateable{
public:
	virtual void update() = 0;
};