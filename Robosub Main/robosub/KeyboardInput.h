#pragma once
#include <memory>
#include <vector>

using namespace std;

class KeyboardListener;

//Singleton for keyboard listening
class KeyboardInput{
	private:
		KeyboardInput();
		KeyboardInput(KeyboardInput const&) = delete;//Don't implement
		void operator=(KeyboardInput const&) = delete;//Don't implement

		vector<shared_ptr<KeyboardListener>> views;
		char val;
	public:
		static KeyboardInput& getKeyboard(){
			static KeyboardInput keyboard;
			return keyboard;
		}
		int attach(shared_ptr<KeyboardListener> obs);
		void detach(int i);
		void setVal(char c);
		char getVal() { return val; }
		void notify();
};