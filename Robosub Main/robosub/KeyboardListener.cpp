#include "KeyboardListener.h"
#include "KeyboardInput.h"


KeyboardListener::KeyboardListener(){
	index = KeyboardInput::getKeyboard().attach(shared_ptr<KeyboardListener>(this));
	mlastChar = '-1';
}

KeyboardListener::~KeyboardListener(){
	KeyboardInput::getKeyboard().detach(index);
}

char KeyboardListener::lastChar(){
	return mlastChar;
};