#pragma once

#include "Vision.h"
#include "Task.h"
#include "Camera.h"
#include "TargetNavigationController.h"
#include "DepthSensor.h"
#include <iostream>
#include <fstream>
#include "Compass.h"


class Task;
/*
Actions

set target
approach
	terminates when target is x% of screan
align
	terminates when target vector is within threshold
manual target
	terminates after set period of time
*/

class Mission{
	private:
		shared_ptr<Vision> vision;
		vector<shared_ptr<Task>> tasks;
		shared_ptr<Camera> forwardCam, downCam;
		shared_ptr<TargetNavigationController> nav;
		shared_ptr<DepthSensor> depthSensor;
		shared_ptr<Compass> compass;
		const string missionFile = "../mission.txt";
		int currentTask;
		bool complete,//mission complete?
			paused,//mission paused?
			visionEdit;//Vision bounds editable?
		unique_ptr<KeyboardListener> key;
	public:
		Mission(shared_ptr<Camera>, shared_ptr<Camera>, shared_ptr<DepthSensor> depthSensor, shared_ptr<Imu::ImuController>, shared_ptr<Compass> compass, vector<shared_ptr<Task>>);
		Mission(shared_ptr<Camera>, shared_ptr<Camera>, shared_ptr<DepthSensor> depthSensor, shared_ptr<Imu::ImuController>, shared_ptr<Compass> compass);
		Mat update();//Process current task, and return image from Vision
		void keyboard();//handle keyboard input
		void cleanup();//What to do after mission is complete

		bool getTasksFromFile();
		bool getTasksFromFile(string f);//Get tasks from file
		bool getVisionEdit(){ return visionEdit; };

		void expandVisionTarget(Scalar target);//Increase bounds for image filtering
		void narrowVisionTarget(Scalar target);//Decrease bounds for image filtering
		void printVisionTarget();

		shared_ptr<Camera> getActiveCamera();
		shared_ptr<Vision> getVision();
};