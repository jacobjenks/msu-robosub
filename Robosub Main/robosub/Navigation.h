#pragma once
#include "MotorController\JrkMotor.hpp"
#include <memory>
#include <iostream>
#include "MessageDisplay.h"

using namespace Jrk;
using namespace std;

enum MotorDirection{
	Forward,
	Backward,
	Stop
};

//Singleton class for holding motors
class Navigation{
	private:
		shared_ptr<JrkMotor> frontStrafe;
		shared_ptr<JrkMotor> backStrafe;
		shared_ptr<JrkMotor> left;
		shared_ptr<JrkMotor> right;
		shared_ptr<JrkMotor> backDepth;
		shared_ptr<JrkMotor> frontDepth;

		uint32_t defaultPower;//motor power
		bool valid;//set to false when a motor fails to initialize, and prevents navigation from being used

		Navigation();
		~Navigation();
		Navigation(Navigation const&) = delete;//Don't implement
		void operator=(Navigation const&) = delete;//Don't implement
	public:
		static Navigation& getNav(){
			static Navigation nav;
			return nav;
		}

		//these functions set motor direction, and return success or failure
		bool setFrontStrafe(MotorDirection);
		bool setBackStrafe(MotorDirection);
		bool setLeft(MotorDirection);
		bool setRight(MotorDirection);
		bool setBackDepth(MotorDirection);
		bool setFrontDepth(MotorDirection);
		bool setAll(MotorDirection);

		bool setMotor(shared_ptr<JrkMotor> motor, MotorDirection direction);

		//Set direction with power between 0,1
		bool setFrontStrafe(MotorDirection, float power);
		bool setBackStrafe(MotorDirection, float power);
		bool setLeft(MotorDirection, float power);
		bool setRight(MotorDirection, float power);
		bool setBackDepth(MotorDirection, float power);
		bool setFrontDepth(MotorDirection, float power);
		bool setAll(MotorDirection, float power);

		bool setMotor(shared_ptr<JrkMotor> motor, MotorDirection direction, float power);

		bool checkPower(float power);//Make sure power is between 0 and 1
		vector<string> getMotorStatus();
};