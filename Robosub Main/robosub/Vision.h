#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.h>
#include <iostream>
#include <memory>
#include "KeyboardListener.h"
#include "Updateable.h"
#include "MessageDisplay.h"
#include "Camera.h"
#include "RUtility.h"
#include "World.h"


using namespace cv;
using namespace std; 

enum VisionImageMode{
	Normal,//draw input image
	Filtered,//Nothing but image filter applied in imageFilter step
	Edges,//draw target edges (canny)
	Contours//draw target contours
};

/*
	This class is responsible for processing input from cameras. It includes functions for finding points of reference
	for navigation, such as the centers of objects, or the direction in which objects are pointed.
*/
class Vision : Updateable{
private:
	shared_ptr <Camera> cam;
	static const int canny_hysteresis_thresh = 100;//Threshold for contour detection
	int min_contour_size = 0;//minimum size for a contour to be accepted
	Scalar lowerHSVBound, upperHSVBound;//define color targets in HSV, H={0-180, S={0,255}, V={0,255}
	Mat processedImage;//image resulting from various processes, depending on what VisionImageMode is set
	/*
	*targetRadius and lastTarget (below) are to be moved to the World class
	*/
	Point3f lastTarget;//last location of the given target
	float targetRadius;//In the WorldObject class, this is being converted to width and height

	VisionImageMode mode;
	unique_ptr<KeyboardListener> key;
	bool visible;
public:
	Vision(shared_ptr<Camera> c);
	Vision(shared_ptr<Camera> c, const Scalar lower, const Scalar upper);
	void setCamera(shared_ptr<Camera> c);
	shared_ptr<Camera> getCamera();
	/* Converted over to World class*/
	Point3f getLastTarget(){ return lastTarget; }
	float getTargetRadius(){ return targetRadius; }
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/	
	Mat getLastFrame();

	void setMode(VisionImageMode);
	void cycleMode();
	void increaseMinContour();
	void decreaseMinContour();

	void setHSVBounds(const Scalar lower, const Scalar upper);//set HSV color bounds for image filtering
	void getHSVBounds(Scalar &lower, Scalar &upper);
	void resetHSVBounds();//Reset bounds to filter out everything

	static Mat preFilter(Mat image);//Filter color and normalize for lighting
	Mat imageFilter(Mat image);//Filter an image by HSV color range
	void getContours(Mat image, vector<vector<Point>>& contours);//Store array of image contours in &contours
	float getOrientation();//get object orientation using PCA. Returns NDC vector pointing in direction of orientation from the origin. Automatically translates perspective
	Point2f getMassCenter(vector<Point> contour);//Get contour mass center

	Mat drawPoint(Mat image, Point3f p, Scalar color);//Draw a point on the provided image
	//void drawLine(Mat& image);//Draw a line on the provided image
	
	/*
		The mat should be parsed to give an enumeration specified in World.h
	*/
	Point3f findTarget();
	Point3f findTarget(bool contiguous);
	Point3f findTarget(Mat image, bool contiguous);

	bool isVisible();//Is the target visible?
	void update();

	Point3f S2NDC(Point3f f, int width, int height);//Screen to NDC
	Point3f NDC2S(Point3f f, int width, int height);//NDC to screen

	void adaptiveFilter(Point2f center, float radius);
};