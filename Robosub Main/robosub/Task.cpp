#include "Task.h"

//Wait constructor
Task::Task(TaskType type, int timeout){
	this->type = type;
	this->timeout = timeout;
	this->lowerBound = Scalar(0,0,0);
	this->upperBound = Scalar(0, 0, 0);
	this->target = Point3f(0, 0, 0);
	this->thrust = Point3f(0, 0, 0);
	this->cam = TaskCam::ForwardCam;
	this->radiusThreshold = 0;
	complete = false;
	started = false;
	start = clock();
	lastRun = 0;
}

Task::Task(TaskType type, int timeout, Point3f target){
	this->type = type;
	this->timeout = timeout;
	this->lowerBound = Scalar(0, 0, 0);
	this->upperBound = Scalar(0, 0, 0);
	this->target = target;
	this->thrust = Point3f(0, 0, 0);
	this->radiusThreshold = 0;
	complete = false;
	started = false;
	start = clock();
	lastRun = 0;
}

Task::Task(TaskType type, TaskCam cam, int timeout, Point2f targetOffset, Scalar lowerBound, Scalar upperBound, float radiusThreshold){
	this->type = type;
	this->timeout = timeout;
	this->lowerBound = lowerBound;
	this->upperBound = upperBound;
	this->target = Point3f(0, 0, 0);
	this->thrust = Point3f(0, 0, 0);
	this->cam = cam;
	this->radiusThreshold = radiusThreshold;
	complete = false;
	started = false;
	start = 0;
	lastRun = 0;
}

bool Task::run(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth){
	switch (type){
		case TaskType::Wait:
			if (wait(nav)){
				complete = true;
				return true;
			}
			break;
		case TaskType::Align:
			if (align(vision, nav, depth)){
				complete = true;
				return true;
			}
			break;
		case TaskType::Approach:
			if (approach(vision, nav, depth)){
				complete = true;
				return true;
			}
			break;
		case TaskType::ManualTarget:
			if (manualTarget(nav, depth)){
				complete = true;
				return true;
			}
			break;
	}
	started = true;
	lastRun = clock();
	return false;
}

bool Task::timedOut(){
	if (timeout == 0)
		return false;
	else if (((float)clock() - start) / CLOCKS_PER_SEC > timeout)
		return true;
	
	return false;
}

bool Task::wait(shared_ptr<TargetNavigationController> nav){
	if (!started){
		MessageDisplay::add(MessageType::message, "Waiting for " + to_string(timeout) + " seconds");
		start = clock();
		nav->setActive(false);
	}
	nav->update(Point3f(0, 0, 0));
	if (timedOut())
		return true;
	
	return false;
}

//Return a thrust vector using PID control
Point3f Task::navPID(Point3f target, float depth){
	float runTime = clock() - lastRun;

	/*** PD control ****/
	//http://www.societyofrobots.com/programming_PID.shtml

	//scale factors
	float P = 1,//Proportional
		D = 0,//Derivative
		E = .2;//Error

	thrust = target;
	//Ignore error less than E
	if (abs(thrust.x) < E)
		thrust.x = 0;
	if (abs(thrust.y) < E)
		thrust.y = 0;

	thrust = P*thrust + D*((lastTarget - thrust) * 1000 * (1 / runTime));

	//depth control overrides other considerations
	if (depth < targetDepth)
		thrust.y = -1;

	return thrust;
}

bool Task::align(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth){
	if (!started){
		MessageDisplay::add(MessageType::message, "Aligning with target");
		vision->setHSVBounds(lowerBound, upperBound);
		nav->setActive(true);
		start = clock();
		nav->unlockHeading();

		if (cam != TaskCam::DownCam){
			MessageDisplay::add(MessageType::message, "Cannot align on forward camera");
			return false;
		}
	}

	float targetAngle = vision->getOrientation();

	if (depth < targetDepth)
		target = Point3f(0, 0, 0);
	else{
		if (90 - abs(targetAngle) < 7){
			MessageDisplay::add(MessageType::message, "Target aligned");
			return true;
		}

		if (targetAngle < 0)//rotate left
			thrust = Point3f(0, -.05, 0);
		else
			thrust = Point3f(0, .05, 0);
	}
	
	nav->update(TNCMode::Rotate, thrust);

	return false;
}


bool Task::approach(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth){
	//Initialize task
	if (!started){
		vision->setHSVBounds(lowerBound, upperBound);
		nav->setActive(true);
		start = clock();
		maxRadius = 0;
		MessageDisplay::add(MessageType::message, "Approaching target");
		nav->unlockHeading();
	}

	//Get vision target
	target = vision->findTarget();

	if (depth < targetDepth){
		nav->update(Point3f(0,-1,0));
	}

	if(vision->isVisible()){
		approachVerify++;
		if (approachVerify >= approachThreshold && vision->getTargetRadius() > radiusThreshold){
			MessageDisplay::add(MessageType::message, "Arrived at target. Radius: " + RUtility::floatToString(vision->getTargetRadius()));
			return true;
		}
		if (vision->getTargetRadius() > maxRadius){
			maxRadius = vision->getTargetRadius();
		}

		//Move towards target
		nav->update(navPID(target, depth));
		lastTarget = target;
		MessageDisplay::add(MessageType::status, "Approaching target");
	}
	else{//Target not visible -> search for target
		if (lastTarget.x > 0){//Target last seen to right
			nav->update(TNCMode::Rotate, Point3f(0, .1, 0));
			MessageDisplay::add(MessageType::status, "Target lost - looking right.");
		}
		else{//Target last seen left
			MessageDisplay::add(MessageType::status, "Target lost - looking left.");
			nav->update(TNCMode::Rotate, Point3f(0, -.1, 0));
		}
	}

	return false;
}

//this task runs until it times out
bool Task::manualTarget(shared_ptr<TargetNavigationController> nav, float depth){
	if (!started){
		nav->setActive(true);
		start = clock();
		MessageDisplay::add(MessageType::message, "Blind navigation for "+to_string(timeout)+" seconds");
		nav->lockHeading();
	}
	nav->update(target);

	if (timedOut())
		return true;
	return false;
}