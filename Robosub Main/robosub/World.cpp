#pragma once

#include "World.h"





World::World() {
	//Instantiate the WorldObjects with set values for constructor
}

Point3f World::getCoordinates(WorldObj obj) {
	switch (obj) {
		default:
			break;
		case(RB):
			return redBuoy.getLocation();
		case(YB):
			return yellowBuoy.getLocation();
		case(GB):
			return greenBuoy.getLocation();
		case(TT):
			return torpedoTarget.getLocation();
		case(PVC):
			return pvcPipe.getLocation();
		case(BB):
			return blackBin.getLocation();
		case(CB):
			return coveredBin.getLocation();
		case(FS):
			return floorSquare.getLocation();
	}
}

void World::setCoordinates(WorldObj obj, Point3f coord) {
	switch (obj) {
		default:
			break;
		case(RB) :
				 return redBuoy.setLocation(coord);
		case(YB) :
			return yellowBuoy.setLocation(coord);
		case(GB) :
			return greenBuoy.setLocation(coord);
		case(TT) :
			return torpedoTarget.setLocation(coord);
		case(PVC) :
			return pvcPipe.setLocation(coord);
		case(BB) :
			return blackBin.setLocation(coord);
		case(CB) :
			return coveredBin.setLocation(coord);
		case(FS) :
			return floorSquare.setLocation(coord);
	}
}