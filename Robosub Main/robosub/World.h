#pragma once 

#include <opencv2/core/core.hpp>
#include <iostream>
#include "WorldObject.h"

using namespace cv;
using namespace std;

/*
WorldObj refers to the world objects stored in the World class.
This should be used when asking for the coordinates of a specific object.
*/
enum WorldObj {
	RB, //red buoy
	YB, //yellow buoy
	GB, //green buoy
	BB, //black bin - uncovered
	CB, //covered black bin
	PVC, //pvc pipe
	TT,//torpedo target
	FS //floorSquare
};

//This class will hold the world objects
class World {
public:
	World();
	Point3f getCoordinates(WorldObj obj);
	void setCoordinates(WorldObj obj, Point3f coord);

	WorldObject redBuoy;
	WorldObject greenBuoy;
	WorldObject yellowBuoy;
	WorldObject pvcPipe;
	WorldObject blackBin;
	WorldObject coveredBin;
	WorldObject floorSquare;
	WorldObject torpedoTarget;
	
};

