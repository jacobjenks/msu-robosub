#include "KeyboardInput.h"
#include "KeyboardListener.h"
#include "Vision.h"
#include "Camera.h"
#include "memory"
#include "DepthSensor.h"
#include "libusb-1.0\libusb.h"
#include "opencv2\highgui\highgui.hpp"
#include "MotorController\JrkMotor.hpp"
#include <time.h>
#include <sstream>
#include "NavigationController.h"
#include "ManualNavigationController.h"
#include "TargetNavigationController.h"
#include "phidget21.h"
#include "Imu.h"
#include "MessageDisplay.h"
#include "Mission.h"
#include "RUtility.h"
#include "Compass.h"


using namespace cv;
using namespace std;

shared_ptr<Camera> cameraForward, cameraDown;
shared_ptr<DepthSensor> depthSensor;
shared_ptr<Compass> compass;
shared_ptr<Imu::ImuController> imu;
bool running = true;
KeyboardListener k;
Mission* mission;
float currentDepth;
bool mouse_L_down, mouse_R_down;

void on_mouse(int event, int x, int y, int flags, void*){
	switch (event){
		case EVENT_LBUTTONDOWN:
			mouse_L_down = true;
			break;
		case EVENT_LBUTTONUP:
			mouse_L_down = false;
			break;
		case EVENT_RBUTTONDOWN:
			mouse_R_down = true;
			break;
		case EVENT_RBUTTONUP:
			mouse_R_down = false;
			break;
	}

	Mat image = mission->getActiveCamera()->getLastFrame();
	image = Vision::preFilter(image);
	if (image.size().width > x && image.size().height > y && x > 0 && y > 0){
		Vec3b color = image.at<Vec3b>(y, x);

		if (mission->getVisionEdit())
			cout << "Mouse HSV: " + to_string(color.val[0]) + ","+ to_string(color.val[1]) +","+ to_string(color.val[2]) << std::endl;

		if (mouse_L_down)
			mission->expandVisionTarget(Scalar(color.val[0], color.val[1], color.val[2]));
		else if (mouse_R_down)
			mission->narrowVisionTarget(Scalar(color.val[0], color.val[1], color.val[2]));
	}
}

void help(){
	MessageDisplay::add(MessageType::message, " ");
	MessageDisplay::add(MessageType::message, "Help: ");
	MessageDisplay::add(MessageType::message, "Press [1][2] to switch cameras");
	MessageDisplay::add(MessageType::message, "Press [\] to pause camera");
	MessageDisplay::add(MessageType::message, "Press [0] to take a screenshot");
	MessageDisplay::add(MessageType::message, "Press [m]to cycle the vision filtering mode");
	MessageDisplay::add(MessageType::message, "Press [<][>] to increase or decrease minimum contour size");
	MessageDisplay::add(MessageType::message, "Press [n] to toggle automatic navigation");
	MessageDisplay::add(MessageType::message, "Press [w][a][s][d] to move, [q][e] to rotate");
	MessageDisplay::add(MessageType::message, "Press [x][space] to ascend/descend");
	MessageDisplay::add(MessageType::message, "Press [l] to toggle vision target lock");
	MessageDisplay::add(MessageType::message, "Press [k] to clear the current vision target");
	MessageDisplay::add(MessageType::message, "Click on the screen to expand/narrow the current vision target");
	MessageDisplay::add(MessageType::message, "Press [p] to pause the mission");
	MessageDisplay::add(MessageType::message, "Press [r] to reload the mission");
	MessageDisplay::add(MessageType::message, "Press [;]['] to switch to the previous/next task");
	MessageDisplay::add(MessageType::message, "Press [-][+] to scroll through these messages");
	MessageDisplay::add(MessageType::message, "Press [esc] to quit");
}

void handleKeyboard(){
	char c;
	if (k.lastChar() != '�')
		c = k.lastChar();
	switch (k.lastChar()){
		case 27://escape key
			running = false;
			break;
		case 'h':
			help();
			break;
		case '-':
			if (MessageDisplay::currentMessage > 0 && MessageDisplay::currentMessage > MessageDisplay::numMessages)
				MessageDisplay::currentMessage--;
			break;
		case '=':
			if (MessageDisplay::currentMessage < MessageDisplay::messageSize() - 1)
				MessageDisplay::currentMessage++;
			break;
	}
}

void updateSensors(){
	//update sensors
	depthSensor->update();
	currentDepth = depthSensor->getData();
	MessageDisplay::add(MessageType::status, "Depth: " + RUtility::floatToString(currentDepth) + " feet");

	//update IMU
	imu->update();
	double* heading = imu->getGyroData();
	double* mag = imu->getMagneticReading();
	double* accel = imu->getAccelData();
	//MessageDisplay::add(MessageType::status, "Gyro: " + RUtility::floatToString(heading[0]) + "," + RUtility::floatToString(heading[1]) + "," + RUtility::floatToString(heading[2]));
	//MessageDisplay::add(MessageType::status, "Mag: " + RUtility::floatToString(mag[0]) + "," + RUtility::floatToString(mag[1]) + "," + RUtility::floatToString(mag[2]));
	//MessageDisplay::add(MessageType::status, "Accel: " + RUtility::floatToString(accel[0]) + "," + RUtility::floatToString(accel[1]) + "," + RUtility::floatToString(accel[2]));
	//MessageDisplay::add(MessageType::status, "Heading: " + RUtility::floatToString(imu->getYaw()));
	//MessageDisplay::add(MessageType::status, "Pitch: " + RUtility::floatToString(imu->getPitch()));
	//MessageDisplay::add(MessageType::status, "Roll: " + RUtility::floatToString(imu->getRoll()));

	//update compass
	compass->update();
	MessageDisplay::add(MessageType::status, "Heading: "+RUtility::floatToString(compass->getData()));

	//update cameras
	cameraForward->update();
	cameraDown->update();
}

void shutdown(){
	destroyAllWindows();
}


int main(int argc, char** argv)
{
	MessageDisplay::add(MessageType::message, "Initializing.");

	mouse_L_down = mouse_R_down = false;

	//create output windows
	namedWindow("Robosub", WINDOW_AUTOSIZE | CV_GUI_EXPANDED);

	//create mouse callbacks
	setMouseCallback("Robosub", on_mouse, 0);

	//add cameras
	cameraForward = shared_ptr<Camera>(new Camera("Camera Forward",2));
	cameraDown = shared_ptr<Camera>(new Camera("Camera Down",1));

	//initialize sensors - don't forget to add \\\\.\\ for COM ports 10 and above
	imu = shared_ptr<Imu::ImuController>(new Imu::ImuController());
	depthSensor = shared_ptr<DepthSensor>(new DepthSensor("COM3"));
	compass = shared_ptr<Compass>(new Compass("\\\\.\\COM17"));
	updateSensors();

	//initialize mission control
	mission = new Mission(cameraForward, cameraDown, depthSensor, imu, compass);
	mission->getTasksFromFile();

	MessageDisplay::add(MessageType::message, "");
	MessageDisplay::add(MessageType::message, "Initialization complete. Press [h] for help.");

	//update all updateable objects
	while (running){
		clock_t start = clock();

		updateSensors();
		Mat output = mission->update();

		KeyboardInput::getKeyboard().setVal(waitKey(30));

		//Current FPS
		stringstream fps;
		fps << "FPS: " << round(1.0f / ((clock() - start) / 1000.0f));
		MessageDisplay::add(MessageType::status, fps.str());

		MessageDisplay::dump(output);
		imshow("Robosub", output);
		
		//This extra waitKey is necessary to allow the screen to render while we are holding down a key
		char w = waitKey(1);
		if (w != -1)
			KeyboardInput::getKeyboard().setVal(w);
		handleKeyboard();
	}
	
	shutdown();
	return 0;
}
