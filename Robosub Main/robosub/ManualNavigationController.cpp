#include "ManualNavigationController.h"

ManualNavigationController::ManualNavigationController(){
	active = true;
	defaultStop = true;
}

ManualNavigationController::ManualNavigationController(bool defaultStop){
	active = true;
	this->defaultStop = defaultStop;
}


//do nothing with inputs
bool ManualNavigationController::update(Point3f target){
	bool updated = false;
	if (active){
		switch (key.lastChar()){
			case 'w':
				Navigation::getNav().setLeft(MotorDirection::Forward);
				Navigation::getNav().setRight(MotorDirection::Forward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual forward");
				break;
			case 'a':
				Navigation::getNav().setFrontStrafe(MotorDirection::Forward);
				Navigation::getNav().setBackStrafe(MotorDirection::Backward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual strafe left");
				break;
			case 's':
				Navigation::getNav().setLeft(MotorDirection::Backward);
				Navigation::getNav().setRight(MotorDirection::Backward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual reverse");
				break;
			case 'd'://not sure how these are oriented, better check in lab
				Navigation::getNav().setFrontStrafe(MotorDirection::Backward);
				Navigation::getNav().setBackStrafe(MotorDirection::Forward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual strafe right");
				break;
			case 'q'://rotate left
				Navigation::getNav().setFrontStrafe(MotorDirection::Forward);
				Navigation::getNav().setBackStrafe(MotorDirection::Forward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual rotate left");
				break;
			case 'e'://rotate right
				Navigation::getNav().setFrontStrafe(MotorDirection::Backward);
				Navigation::getNav().setBackStrafe(MotorDirection::Backward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual rotate right");
				break;
			case 32://space bar -> ascend
				Navigation::getNav().setFrontDepth(MotorDirection::Forward);
				Navigation::getNav().setBackDepth(MotorDirection::Forward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual ascend");
				break;
			case 'x'://descend
				Navigation::getNav().setFrontDepth(MotorDirection::Backward);
				Navigation::getNav().setBackDepth(MotorDirection::Backward);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual descend");
				break;
			case 't'://full stop
				Navigation::getNav().setAll(MotorDirection::Stop);
				updated = true;
				//MessageDisplay::add(MessageType::message, "Manual full stop");
				break;
			default://full stop on no input
				if (defaultStop)
					Navigation::getNav().setAll(MotorDirection::Stop);
				break;
		}
	}
	return updated;
}