#pragma once

#include <vector>
#include <opencv/cv.h>
#include "KeyboardListener.h"
#include <time.h>
#include "Navigation.h"
#include <ctime>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>

using namespace std;
using namespace cv;

enum MessageType{
	status,//sensor readings etc, shown on bottom right. Updated every frame.
	message//error messages and current activities. Shows a limited number of messages, after which the oldest message expires
};

class MessageDisplay{
	private:
		static clock_t start;
		static vector<string> status, message, output;
		static const int fontLineSize = 17;
		static string logFileName;
		MessageDisplay();
		static void printToFile();
	public:
		static int currentMessage;
		static const int numMessages = 10;
		static void add(MessageType t, string s);
		static void dump(Mat image);//add messages to image, delete messages, and return image
		static void update();
		static int messageSize();
};