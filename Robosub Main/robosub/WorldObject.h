#pragma once

#include <opencv2/core/core.hpp>
#include <iostream>

using namespace cv;
using namespace std;

class WorldObject {
private:
	Point3f location;
	float width;
	float height;
public:
	WorldObject();
	WorldObject(float inWidth, float inHeight);
	Point3f getLocation();
	void setLocation(Point3f loc);
};