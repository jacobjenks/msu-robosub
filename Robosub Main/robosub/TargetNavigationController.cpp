#include "TargetNavigationController.h"

/*
	@param width: Width of target coordinate space
	@param height: Height of target coordinate space
*/
TargetNavigationController::TargetNavigationController(shared_ptr<DepthSensor> depthSensor, shared_ptr<Compass> compass){
	this->depthSensor = depthSensor;
	this->compass = compass;
	lockedHeading = compass->getData();
	lockedDepth = depthSensor->getData();
	active = false;
	output = true;
	manual = unique_ptr<ManualNavigationController>(new ManualNavigationController(false));
	Navigation::getNav();
	correctionMode = false;
	minDepthMode = false;
	clockwiseHeadingCorrection = false;
	useHeadingLock = false;
	useDepthLock = false;
}

void TargetNavigationController::setActive(bool b){
	active = b;
}

bool TargetNavigationController::update(Point3f target){
	return update(TNCMode::Thrust, target);
}

void TargetNavigationController::lockHeading(){
	useHeadingLock = true;
}

void TargetNavigationController::unlockHeading(){
	useHeadingLock = false;
}

float TargetNavigationController::getHeadingError(){
	//Calculate heading
	double yaw = compass->getData();
	double oldYaw = lockedHeading;
	double error;

	double dif = yaw - oldYaw;
	double absDif = std::abs(dif);

	if (absDif <= 180)
		error = absDif == 180 ? absDif : dif;
	else if (yaw > oldYaw)
		error = absDif - 360;
	else
		error = 360 - absDif;

	return error;
}

//Return true if we have drifted from desired depth or heading
bool TargetNavigationController::driftCheck(){
	//check for depth drift
	if (useDepthLock && std::abs(depthSensor->getData() - lockedDepth) > depthTolerance)
		return true;

	//Check for heading drift
	if (useHeadingLock && std::abs(getHeadingError()) > headingTolerance){
		//If we aren't already in correction mode, indicate which direction we need to move in to correct
		if (!correctionMode){
			if (error > 0)
				clockwiseHeadingCorrection = false;
			else
				clockwiseHeadingCorrection = true;
		}
		return true;
	}

	return false;
}

/*
*	The sub likes to veer to the right, and is positively buoyant. 
*
*	This function checks to see if the sub has drifted from its desired heading and depth, and returns a corrected target for it.
*	Once a deviation is detected the sub then switches to a correction mode, signified by minDepthMode or correctionMode.
*
*	In the case of a heading correction, the sub will turn until it passes its locked heading again, to ensure the sub doesn't just slightly turn
*	in the direction of the error. Depth correction will run until the sub is deeper than the desired depth, since it tends to float up.
*
*	The sub does not leave correction mode until it is within half the tolerance it uses to detect a deviation. This gives the sub some leeway when
*	navigating so it doesn't have to correct every half second, but ensures that when we do correct it does a good job of it.
*/
void TargetNavigationController::driftCorrection(TNCMode *mode, Point3f *target){
	//Update depth and heading lock
	if (*mode == TNCMode::Rotate)
		lockedHeading = compass->getData();
	else if ((*target).y != 0)
		lockedDepth = depthSensor->getData();

	MessageDisplay::add(MessageType::status, "Heading lock: " + RUtility::floatToString(lockedHeading));
	MessageDisplay::add(MessageType::status, "Depth lock: " + RUtility::floatToString(lockedDepth));

	//Ensure we maintain min depth above all else
	if ((minDepthMode && depthSensor->getData() < minDepth + depthTolerance) ||//Go deeper than min depth if we're in minDepthMode
			depthSensor->getData() < minDepth){//Otherwise just ensure we're at min depth
		minDepthMode = true;
		MessageDisplay::add(MessageType::status, "Nav: minimum depth");
		(*target).y = -1;
		(*target).x = 0;
		(*target).z = 0;
		return;
	}
	else if (minDepthMode && depthSensor->getData() >= minDepth + depthTolerance)
		minDepthMode = false;

	//Check to see if we have drifted, and correct if we have
	if (driftCheck())
		correctionMode = true;

	//Correct for drift. Correction uses tighter tolerance than checking for drift
	if (correctionMode){
		correctionMode = false;//Leave correction mode if no further infractions are found

		//Correct for depth before heading
		if (useDepthLock && 
			(depthSensor->getData() > lockedDepth ||//Ensure we end up below lockedDepth, since sub is positively buoyant
				std::abs(depthSensor->getData() - lockedDepth) > depthTolerance / 2))
		{
			MessageDisplay::add(MessageType::status, "Nav: depth drift");
			*mode = TNCMode::Thrust;
			if (depthSensor->getData() < lockedDepth)//drifted up
				(*target).y = -1;
			else//drifted down
				(*target).y = 1;

			(*target).x = 0;
			(*target).z = 0;
			correctionMode = true;
			return;//Depth concerns override heading problems
		}

		//Don't use heading correction
		return;

		//Correct for heading
		if (useHeadingLock &&
			(clockwiseHeadingCorrection && getHeadingError() < 0 ||//We drifted cclockwise, and we haven't yet corrected past the locked heading
				!clockwiseHeadingCorrection && getHeadingError() > 0 ||//Drifted clockwise, and we haven't yet corrected past the locked heading
				std::abs(getHeadingError()) > headingTolerance/2))//Our current heading still deviates too far from locked heading
		{
			*mode = TNCMode::Rotate;
			if (getHeadingError() > 0){//Drifted clockwise
				MessageDisplay::add(MessageType::status, "Nav: clockwise heading drift");
				*target = Point3f(0, -.1, 0);
			}
			else{//Drifted counterclockwise
				MessageDisplay::add(MessageType::status, "Nav: counterclockwise heading drift");
				*target = Point3f(0, .1, 0);
			}
			correctionMode = true;
		}
	}
	else
		MessageDisplay::add(MessageType::status, "Nav: on target");
}

/*
	Navigate the sub using a given target vector
	Defers to a ManualNavigationController to make sub easier to control
	@param mode: TNCMode to use
	@param target: Target direction to move given by Vision class in terms of a normalized 3D vector
*/
bool TargetNavigationController::update(TNCMode mode, Point3f target){
	//defer to manual control
	if (manual->update(target)){
		lockedHeading = compass->getData();
		lockedDepth = depthSensor->getData();
		return false;
	}

	string message = "";

	//Check for keypresses
	if (key.lastChar() == 'n'){
		if (active){
			MessageDisplay::add(MessageType::message, "TNC disabled");
			active = false;
		}
		else{
			MessageDisplay::add(MessageType::message, "TNC enabled");
			active = true;
		}
	}

	driftCorrection(&mode, &target);

	//Update thrusters
	if (active){
		//check for valid target
		if (target.x > 1 ||
			target.x < -1 ||
			target.y > 1 ||
			target.y < -1 ||
			target.z > 1 ||
			target.z < -1){
			Navigation::getNav().setAll(MotorDirection::Stop);
			if (output)
				MessageDisplay::add(MessageType::message, "TNC: Invalid target");
			return true;
		}

		//We discovered that running more than two motors at once on low battery doesn't work so well,
		//so we'll just run the motors on one axis at a time
		if (mode == TNCMode::Thrust){
			if (abs(target.x) > alignThreshold || abs(target.y) > alignThreshold){
				if (abs(target.x) > abs(target.y))
					horizontal(target.x);
				else
					vertical(target.y);
			}
			else
				forward(target.z);
		}
		else if (mode == TNCMode::Rotate)
			rotate(target);
		
		/*
		//Trying more than one pair at a time. Remember to fix motor shutoff in hor, vert, forw functions if enabling this
		if (mode == TNCMode::Thrust){
			horizontal(target.x);
			vertical(target.y);
			forward(target.z);
		}
		else if (mode == TNCMode::Rotate)
			rotate(target);
		*/

		return true;
	}
	else
		Navigation::getNav().setAll(MotorDirection::Stop);

}

string TargetNavigationController::forward(float z){
	Navigation::getNav().setFrontDepth(MotorDirection::Stop);
	Navigation::getNav().setBackDepth(MotorDirection::Stop);
	Navigation::getNav().setFrontStrafe(MotorDirection::Stop);
	Navigation::getNav().setBackStrafe(MotorDirection::Stop);

	if (z > 0){//forward
		Navigation::getNav().setLeft(MotorDirection::Forward, abs(z));
		Navigation::getNav().setRight(MotorDirection::Forward, abs(z));
		return "Forward";
	}
	else if (z < 0){//backward
		Navigation::getNav().setLeft(MotorDirection::Backward, abs(z));
		Navigation::getNav().setRight(MotorDirection::Backward, abs(z));
		return "Backward";
	}
	else{
		Navigation::getNav().setLeft(MotorDirection::Stop);
		Navigation::getNav().setRight(MotorDirection::Stop);
	}
	return "";
}

string TargetNavigationController::vertical(float y){
	Navigation::getNav().setLeft(MotorDirection::Stop);
	Navigation::getNav().setRight(MotorDirection::Stop);
	Navigation::getNav().setFrontStrafe(MotorDirection::Stop);
	Navigation::getNav().setBackStrafe(MotorDirection::Stop);

	if (y > 0){//ascend
		Navigation::getNav().setFrontDepth(MotorDirection::Forward, abs(y));
		Navigation::getNav().setBackDepth(MotorDirection::Forward, abs(y));
		return "Ascend ";
	}
	else if (y < 0){//descend
		Navigation::getNav().setFrontDepth(MotorDirection::Backward, abs(y));
		Navigation::getNav().setBackDepth(MotorDirection::Backward, abs(y));
		return "Descend ";
	}
	else{
		Navigation::getNav().setFrontDepth(MotorDirection::Stop);
		Navigation::getNav().setBackDepth(MotorDirection::Stop);
	}
	return "";
}

string TargetNavigationController::horizontal(float x){
	Navigation::getNav().setFrontDepth(MotorDirection::Stop);
	Navigation::getNav().setBackDepth(MotorDirection::Stop);
	Navigation::getNav().setLeft(MotorDirection::Stop);
	Navigation::getNav().setRight(MotorDirection::Stop);

	if (x < 0){//left
		Navigation::getNav().setFrontStrafe(MotorDirection::Forward, abs(x));
		Navigation::getNav().setBackStrafe(MotorDirection::Backward, abs(x));
		return "Left ";
	}
	else if (x > 0){//right
		Navigation::getNav().setFrontStrafe(MotorDirection::Backward, abs(x));
		Navigation::getNav().setBackStrafe(MotorDirection::Forward, abs(x));
		return "Right ";
	}
	else{
		Navigation::getNav().setFrontStrafe(MotorDirection::Stop);
		Navigation::getNav().setBackStrafe(MotorDirection::Stop);
	}
	return "";
}

void TargetNavigationController::rotate(Point3f target){

	Navigation::getNav().setFrontDepth(MotorDirection::Stop);
	Navigation::getNav().setBackDepth(MotorDirection::Stop);
	Navigation::getNav().setLeft(MotorDirection::Stop);
	Navigation::getNav().setRight(MotorDirection::Stop);

	if (target.y > 0){//rotate right
		Navigation::getNav().setFrontStrafe(MotorDirection::Backward, abs(target.y));
		Navigation::getNav().setBackStrafe(MotorDirection::Backward, abs(target.y));
	}
	else if (target.y < 0){//rotate left
		Navigation::getNav().setFrontStrafe(MotorDirection::Forward, abs(target.y));
		Navigation::getNav().setBackStrafe(MotorDirection::Forward, abs(target.y));
	}
	else{
		Navigation::getNav().setFrontStrafe(MotorDirection::Stop);
		Navigation::getNav().setBackStrafe(MotorDirection::Stop);
	}
}