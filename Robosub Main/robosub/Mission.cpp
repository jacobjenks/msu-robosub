#include "Mission.h"


Mission::Mission(shared_ptr<Camera> forward, shared_ptr<Camera> down, shared_ptr<DepthSensor> depthSensor, shared_ptr<Imu::ImuController> imu, shared_ptr<Compass> compass, vector<shared_ptr<Task>> tasks){
	this->forwardCam = forward;
	this->depthSensor = depthSensor;
	this->compass = compass;
	this->downCam = down;
	this->vision = shared_ptr<Vision>(new Vision(forwardCam));
	this->tasks = tasks;
	this->nav = shared_ptr<TargetNavigationController>(new TargetNavigationController(depthSensor, compass));
	this->currentTask = 0;
	this->complete = false;
	key = unique_ptr<KeyboardListener>(new KeyboardListener());
	paused = false;
	visionEdit = false;
	nav->lockHeading();
}

Mission::Mission(shared_ptr<Camera> forward, shared_ptr<Camera> down, shared_ptr<DepthSensor> depthSensor, shared_ptr<Imu::ImuController> imu, shared_ptr<Compass> compass){
	this->forwardCam = forward;
	this->depthSensor = depthSensor;
	this->compass = compass;
	this->downCam = down;
	this->vision = shared_ptr<Vision>(new Vision(forwardCam));
	this->nav = shared_ptr<TargetNavigationController>(new TargetNavigationController(depthSensor, compass));
	this->currentTask = 0;
	this->complete = false;
	key = unique_ptr<KeyboardListener>(new KeyboardListener());
	paused = false;
	visionEdit = false;
	nav->lockHeading();
}

//Process current task
Mat Mission::update(){
	keyboard();
	vision->update();

	if (!paused && currentTask < tasks.size()){
		//set proper camera
		if (!tasks.at(currentTask)->isStarted()){
			if (tasks.at(currentTask)->getCam() == TaskCam::DownCam)
				vision->setCamera(downCam);
			else
				vision->setCamera(forwardCam);
		}

		//run task
		if (tasks.at(currentTask)->run(vision, nav, depthSensor->getData()))
			currentTask++;
	}
	else if (!paused && !complete){
		complete = true;
		cleanup();
	}

	if (complete || paused){
		nav->update(Point3f(0, 0, 0));	
		vision->findTarget();
	}

	Mat display = vision->getLastFrame();
	//Draw current target location
	/*
	display = vision->drawPoint(display,
		currentTask < tasks.size() ? tasks.at(currentTask)->getTarget() : Point3f(0, 0, 0),
		Scalar(255,0,0));
	*/

	//Draw current thrust target
	/*display = vision->drawPoint(display,
		currentTask < tasks.size() ? tasks.at(currentTask)->getThrust() : Point3f(0, 0, 0),
		Scalar(255,0,0));
	*/

	if (visionEdit)
		printVisionTarget();

	return display;
}

void Mission::printVisionTarget(){
	Scalar lowerHSVBound, upperHSVBound;
	vision->getHSVBounds(lowerHSVBound, upperHSVBound);
	MessageDisplay::add(MessageType::status, "Target Lower: " + RUtility::floatToString(lowerHSVBound[0]) + ","
		+ RUtility::floatToString(lowerHSVBound[1]) + ","
		+ RUtility::floatToString(lowerHSVBound[2]));

	MessageDisplay::add(MessageType::status, "Target Upper: " + RUtility::floatToString(upperHSVBound[0]) + ","
		+ RUtility::floatToString(upperHSVBound[1]) + ","
		+ RUtility::floatToString(upperHSVBound[2]));
}

void Mission::expandVisionTarget(Scalar target){
	if (visionEdit){
		Scalar l, u;
		vision->getHSVBounds(l, u);
		if (target[0] < l[0])
			l[0] = target[0];
		if (target[1] < l[1])
			l[1] = target[1];
		if (target[2] < l[2])
			l[2] = target[2];

		if (target[0] > u[0])
			u[0] = target[0];
		if (target[1] > u[1])
			u[1] = target[1];
		if (target[2] > u[2])
			u[2] = target[2];

		vision->setHSVBounds(l, u);
	}
}

void Mission::narrowVisionTarget(Scalar target){
	if (visionEdit){
		Scalar l, u;
		float hMag, sMag, vMag;
		vision->getHSVBounds(l, u);

		if (target[0] - l[0] > u[0] - target[0])
			u[0] = target[0] - 1;
		else
			l[0] = target[0] + 1;

		if (target[1] - l[1] > u[1] - target[1])
			u[1] = target[1] - 1;
		else
			l[1] = target[1] + 1;

		if (target[2] - l[2] > u[2] - target[2])
			u[2] = target[2] - 1;
		else
			l[2] = target[2] + 1;

		vision->setHSVBounds(l, u);
	}
}

void Mission::cleanup(){
	MessageDisplay::add(MessageType::message, "Mission complete");
	vision->setHSVBounds(Scalar(0, 0, 0), Scalar(0, 0, 0));
	nav->setActive(false);
}

shared_ptr<Camera> Mission::getActiveCamera(){
	return vision->getCamera();
}

shared_ptr<Vision> Mission::getVision(){
	return vision;
}

void Mission::keyboard(){
	switch (key->lastChar()){
		case 59://;
			if (currentTask > 0){
				complete = false;
				MessageDisplay::add(MessageType::message, "Switching to task " + to_string(--currentTask));
				tasks.at(currentTask)->setComplete(false);
			}
			break;
		case 39://'
			if (currentTask < tasks.size() - 1){
				MessageDisplay::add(MessageType::message, "Switching to task " + to_string(++currentTask));
				tasks.at(currentTask)->setComplete(false);
			}
			break;
		case '1':
			vision->setCamera(forwardCam);
			break;
		case '2':
			vision->setCamera(downCam);
			break;
		case 'l':
			if (visionEdit){
				visionEdit = false;
				MessageDisplay::add(MessageType::message, "Vision target locked");
			}
			else{
				visionEdit = true;
				MessageDisplay::add(MessageType::message, "Vision target unlocked");
			}
			break;
		case 'k':
			if (visionEdit){
				vision->resetHSVBounds();
				MessageDisplay::add(MessageType::message, "Vision target cleared");
			}
			break;
		case 'p':
			if (paused){
				paused = false;
				MessageDisplay::add(MessageType::message, "Mission unpaused");
			}
			else{
				paused = true;
				MessageDisplay::add(MessageType::message, "Mission paused");
			}
			break;
		case 'r':
			MessageDisplay::add(MessageType::message, "Reloading mission file");
			currentTask = 0;
			getTasksFromFile();
			break;
	}
}

bool Mission::getTasksFromFile(){
	return getTasksFromFile(missionFile);
}

bool Mission::getTasksFromFile(string f){
	tasks.clear();

	ifstream file;
	file.open(f);
	string line;

	vector<string> taskData;

	//get line
	while (getline(file, line, '\n')){
		taskData.clear();
		stringstream s(line);
		string data;
		
		//get each element
		while (getline(s, data, ','))
			taskData.push_back(data);

		shared_ptr<Task> task;
		
		//figure out which constructor to use
		switch (taskData.size()){
			case 2://wait
				tasks.push_back(shared_ptr<Task>(new Task((TaskType)stoi(taskData.at(0)), stof(taskData.at(1)))));
				break;
			case 5://manual target
				tasks.push_back(shared_ptr<Task>(new Task((TaskType)stoi(taskData.at(0)),//type
					stof(taskData.at(1)),//timeout
					Point3f(stof(taskData.at(2)), stof(taskData.at(3)), stof(taskData.at(4)))//Target Coords
					)));
				break;
			case 11://align
				tasks.push_back(shared_ptr<Task>(new Task((TaskType)stoi(taskData.at(0)),//type
					(TaskCam)stoi(taskData.at(1)),//cam
					stof(taskData.at(2)),//timeout
					Point2f(stof(taskData.at(3)), stof(taskData.at(4))),//offset
					Scalar(stof(taskData.at(5)), stof(taskData.at(6)), stof(taskData.at(7))),//Lower HSV bound
					Scalar(stof(taskData.at(8)), stof(taskData.at(9)), stof(taskData.at(10))),//Upper HSV bound
					0
					)));
				break;
			case 12://approach
				tasks.push_back(shared_ptr<Task>(new Task((TaskType)stoi(taskData.at(0)),//type
					(TaskCam)stoi(taskData.at(1)),//cam
					stof(taskData.at(2)),//timeout
					Point2f(stof(taskData.at(3)), stof(taskData.at(4))),//offset
					Scalar(stof(taskData.at(5)), stof(taskData.at(6)), stof(taskData.at(7))),//Lower HSV bound
					Scalar(stof(taskData.at(8)), stof(taskData.at(9)), stof(taskData.at(10))),//Upper HSV bound
					stof(taskData.at(11))
					)));
				break;
		}
	}
	
	return true;
}