#pragma once
#include "NavigationController.h"
#include "KeyboardInput.h"
#include "KeyboardListener.h"

class ManualNavigationController : NavigationController{
	private:
		KeyboardListener key;
		bool active, defaultStop;
	public:
		ManualNavigationController();
		ManualNavigationController(bool defaultStop);
		bool update(Point3f);
};