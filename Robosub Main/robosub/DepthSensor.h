#pragma once

#include "SerialSensor.h"


class DepthSensor : SerialSensor{
private:
	float getDepthSensor();
	float getDepthFeet();
	float sensorToDepth(float val);
	float sensorToPSI(float val);

	float surfacePSI;
	float currentDepth;

#ifdef _WIN32
	//Get various information about the connection
	COMSTAT status;
#endif
public:
	DepthSensor(char *portName);
	
	float getData();
	void update();
};