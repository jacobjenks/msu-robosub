#pragma once
#include "NavigationController.h"
#include "KeyboardInput.h"
#include "KeyboardListener.h"
#include "ManualNavigationController.h"
#include "Imu.h"
#include "DepthSensor.h"
#include "RUtility.h"
#include "Compass.h"

enum TNCMode{
	Thrust,
	Rotate
};

class TargetNavigationController : NavigationController{
private:
	KeyboardListener key;
	bool active, output;
	double alignThreshold = .2;//Threshold for lining up x and y axis
	int width, height;
	double lockedHeading;//Last desired heading. Assume heading only should change during Rotate mode
	double lockedDepth;//Last desired depth.
	const double headingTolerance = 5;//Amount we allow heading to vary from lockedHeading when in Thrust mode
	const double minDepth = 2;
	const double depthTolerance = .5;
	shared_ptr<Compass> compass;
	shared_ptr<DepthSensor> depthSensor;
	unique_ptr<ManualNavigationController> manual;
	bool correctionMode,//whether or not we're currently correcting for heading or depth lock
		minDepthMode,//whether or not we're currently correcting for min depth
		clockwiseHeadingCorrection,//indicates which direction we're correcting for heading drift in
		useHeadingLock,
		useDepthLock;
public:
	TargetNavigationController(shared_ptr<DepthSensor>, shared_ptr<Compass>);
	bool update(Point3f target);
	bool update(TNCMode mode, Point3f target);
	void setActive(bool);
	float getHeadingError();
	bool driftCheck();
	void driftCorrection(TNCMode *, Point3f *);
	string horizontal(float x);
	string vertical(float x);
	string forward(float x);
	void rotate(Point3f target);//Rotate on x axis, maintain depth on y, nothing on z
	void lockHeading();//Lock current heading
	void unlockHeading();//Don't use heading lock
};