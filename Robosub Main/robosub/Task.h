#pragma once
#include "Mission.h"
#include "RUtility.h"
#include <xtgmath.h>

class Mission;
class TargetNavigationController;

enum TaskType{
	Wait,
	Align,
	Approach,
	ManualTarget
};

enum TaskCam{
	ForwardCam,
	DownCam
};

/*
	TODO
		targetOffset
		timeouts
*/

class Task{
	private:
		shared_ptr<Mission> mission;
		TaskType type;
		TaskCam cam;
		int timeout;//time after which task will fail
		Point2f targetOffset;//x and y offset from center of target
		Point3f target, lastTarget, thrust;//NDC target vector
		float targetDepth = 2;
		Scalar lowerBound, upperBound;//Target HSV values
		bool started, complete;
		float radiusThreshold;//Threshold for which we consider to have 'approached' a target.
		clock_t start, lastRun;//When the task started, and when it last ran
		float maxRadius;
		int approachVerify = 0, approachThreshold = 3;//Count the number of consecutive frames the target has the desired apparent size
	public:
		Task(TaskType type, int timeout);//Wait constructor
		Task(TaskType type, int timeout, Point3f target);//ManualTarget constructor
		Task(TaskType type, TaskCam cam, int timeout, Point2f targetOffset, Scalar minTarget, Scalar maxTarget, float radiusThreshold);
		TaskType getType(){ return type; }
		TaskCam getCam(){ return cam; }
		Point3f getTarget(){ return target; }
		Point3f getThrust(){ return thrust; }
		bool isStarted(){ return started; }

		bool isComplete(){ return complete; }
		void setComplete(bool c){ complete = c; }

		Point3f navPID(Point3f target, float depth);

		bool run(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth);//Run the task. Returns true when task is complete. Each task should handle its own failure independently.
		bool timedOut();//Returns true if task has timed out
		bool wait(shared_ptr<TargetNavigationController> nav);//Wait for timeout seconds
		bool align(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth);//Align with given orientation target
		bool approach(shared_ptr<Vision> vision, shared_ptr<TargetNavigationController> nav, float depth);//Ram target
		bool manualTarget(shared_ptr<TargetNavigationController> nav, float depth);
};