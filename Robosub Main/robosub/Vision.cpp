#pragma once

#include "Vision.h"


Vision::Vision(shared_ptr<Camera> c){
	this->cam = c;
	this->resetHSVBounds();
	mode = VisionImageMode::Normal;
	key = unique_ptr<KeyboardListener>(new KeyboardListener());
	lastTarget = Point3f(0, 0, 0);//Moved to World class
	visible = false;
}


Vision::Vision(shared_ptr<Camera> c, Scalar lower, Scalar upper){
	this->cam = c;
	this->lowerHSVBound = lower;
	this->upperHSVBound = upper;
	mode = VisionImageMode::Normal;
	key = unique_ptr<KeyboardListener>(new KeyboardListener());
	lastTarget = Point3f(0, 0, 0); //Moved to World class
	visible = false;
}

void Vision::setCamera(shared_ptr<Camera> c){
	this->cam = c;
}

shared_ptr<Camera> Vision::getCamera(){
	return this->cam;
}

void Vision::setMode(VisionImageMode mode){
	this->mode = mode;
}

void Vision::cycleMode(){
	//This is just an easy way to switch between detailed and simple VisionMode cycling
	if (false){
		if (this->mode == VisionImageMode::Normal)
			this->mode = VisionImageMode::Filtered;
		else
			this->mode = VisionImageMode::Normal;
	}
	else{
		if (this->mode == VisionImageMode::Contours)
			this->mode = VisionImageMode::Normal;
		else
			this->mode = static_cast<VisionImageMode>((int)this->mode + 1);
	}
	
}

void Vision::increaseMinContour(){
	min_contour_size += 10;
	MessageDisplay::add(MessageType::message, "Min contour: " + to_string(min_contour_size));
}

void Vision::decreaseMinContour(){
	min_contour_size -= 10;
	if (min_contour_size < 0)
		min_contour_size = 0;
	MessageDisplay::add(MessageType::message, "Min contour: " + to_string(min_contour_size));
}

Mat Vision::preFilter(Mat image){
	cvtColor(image, image, CV_BGR2HSV, 0);//convert image to HSV

	//Equalize to hopefully take care of lighting problems
	vector<Mat> channels;
	split(image, channels);
	equalizeHist(channels[2], channels[2]);//Equalize on V
	merge(channels, image);

	return image;
}


/*
	Filter the provided image by color range, and convert to HSV format. Returned image will also be blurred to reduce image noise.
*/
Mat Vision::imageFilter(Mat image){
	image = preFilter(image);

	//Filter image by target range
	//Split into multiple filters if we go all the way around the color scale
	if (lowerHSVBound[0] > upperHSVBound[0]){
		inRange(image, Scalar(0, lowerHSVBound[1], lowerHSVBound[2]), upperHSVBound, image);
		Mat temp = image.clone();
		inRange(image, lowerHSVBound, Scalar(255, upperHSVBound[1], upperHSVBound[2]), temp);
		addWeighted(temp, 1, image, 1, 0.0f, image);
	}
	else
		inRange(image, lowerHSVBound, upperHSVBound, image);

	if (mode == VisionImageMode::Filtered){
		processedImage = image.clone();
		//cvtColor(processedImage, processedImage, CV_HSV2BGR, 0);
	}
	
	//blur(image, image, Size(3, 3));

	//Erode dilate to reduce noise
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(2, 2));
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

	erode(image, image, erodeElement);
	erode(image, image, erodeElement);

	dilate(image, image, dilateElement);
	dilate(image, image, dilateElement);

	return image;
}


/*
	Return a point containing the center of mass for a given contour using Opencv's Moments class.
	http://docs.opencv.org/doc/tutorials/imgproc/shapedescriptors/moments/moments.html#moments
*/
Point2f Vision::getMassCenter(vector<Point> contour){
	Moments m = moments(contour, false);
	Point2f result(m.m10 / m.m00, m.m01 / m.m00);
	return result;
}

/*
	Draw a point on the provided image. p is in NDC, so we have to convert back to screen coords
*/
Mat Vision::drawPoint(Mat image, Point3f p, Scalar color){
	p = NDC2S(p, image.size().width, image.size().height);
	circle(image, Point2f(p.x, p.y), 3, color, 5, 8, 0);
	return image;
}

void Vision::setHSVBounds(Scalar lower, Scalar upper){
	this->lowerHSVBound = lower;
	this->upperHSVBound = upper;

	//this->lowerHSVBound = Scalar(lower[0], lower[1], lowerHSVBound[2]);
	//this->upperHSVBound = Scalar(upper[0], upper[1], upperHSVBound[2]);

	this->targetRadius = 0;//Moved to the World class
	this->visible = false;
}

void Vision::getHSVBounds(Scalar &lower, Scalar &upper){
	lower = this->lowerHSVBound;
	upper = this->upperHSVBound;
}

void Vision::resetHSVBounds(){
	this->lowerHSVBound = Scalar(180, 255, 255);
	this->upperHSVBound = Scalar(0, 0, 0);
	
	//going to try ignoring V channel for now to take care of lighting
	//this->lowerHSVBound = Scalar(180, 255, 0);
	//this->upperHSVBound = Scalar(0, 0, 255);
}


/*
	Find the contours of an image filtered using imageFilter()
*/
void Vision::getContours(Mat image, vector<vector<Point>>& contours){
	vector<Vec4i> hierachy;
	vector<vector<Point>> temp;
	contours.clear();

	Canny(image, image, canny_hysteresis_thresh, canny_hysteresis_thresh * 2, 3, true);

	if (mode == VisionImageMode::Edges)
		processedImage = image.clone();

	findContours(image, temp, hierachy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);


	//remove contours under the minimum threshold
	for (int i = 0; i < temp.size(); i++){
		if (temp[i].size() > min_contour_size){
			contours.push_back(temp[i]);
		}
	}

	if (mode == VisionImageMode::Contours){
		processedImage = Mat::zeros(image.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
			drawContours(processedImage, contours, i, Scalar(255, 255, 255), 1, 8, hierachy, 0, Point());
	}
}

//Is the target visible?
bool Vision::isVisible(){
	return visible;
}

//Screen to NDC
Point3f Vision::S2NDC(Point3f p, int width, int height){
	Point3f result;
	result.x = (2 * p.x / width) - 1;
	result.y = ((2 * p.y / height) - 1)*-1;
	result.z = 0;
	return result;
}

//NDC to Screen
Point3f Vision::NDC2S(Point3f p, int width, int height){
	Point3f result;
	result.x = (p.x + 1)*width / 2;
	result.y = (height - (p.y + 1)*height / 2);
	result.z = p.z;
	return result;
}

Point3f Vision::findTarget(){
	return findTarget(cam->getLastFrame(), false);
}


/*
	Return a NDC Point3f giving the center of the target specified by the class constructor
	@param image: Mat object to find the target in
	@contiguous: Whether or not the target should be one contiguous object

	This method may be converted to the World class. 
	The Mat needs to be parsed to suit one of the enumerations defined in World.h
*/
Point3f Vision::findTarget(Mat image, bool contiguous){
	image = imageFilter(image.clone());
	
	//get image contours
	vector<vector<Point>> contours;
	getContours(image, contours);

	//return early if target isn't visible
	if (contours.size() == 0){
		visible = false;
		lastTarget = Point3f(0, 0, 0);
		return lastTarget;
	}
	visible = true;

	vector<Point> biggest = contours.at(0);
	vector<Point> all = contours.at(0);//Using all instead of biggest would be best if we could make sure we filtered everything but the buoy
	for (int i = 1; i < contours.size(); i++){
		all.insert(all.end(), contours.at(i).begin(), contours.at(i).end());
		if (contours.at(i).size() > biggest.size())
			biggest = contours.at(i);
	}
	Point2f center;
	float radius;
	if (contiguous)
		minEnclosingCircle((Mat)biggest, center, radius);
	else
		minEnclosingCircle((Mat)all, center, radius);
	targetRadius = radius;

	//Draw bounding circle
	Point3f result(center.x, center.y, 0);
	circle(processedImage, Point(result.x, result.y), radius, Scalar(0,255,0), 1, 8, 0);

	//Convert result to NDC
	result = S2NDC(result, image.size().width, image.size().height);
	result.z = 1;//Forward thrust because target is in vision
	
	lastTarget = result;

	//adaptiveFilter(center, radius);
	//floodFill(processedImage, center, Scalar(1, 1, 1), 0, Scalar(5, 8, 8), Scalar(5, 8, 8));

	return result;
}

/*
I originally used PCA to find object orientation, but I found that just finding a minAreaRect was much easier
https://robospace.wordpress.com/2013/10/09/object-orientation-principal-component-analysis-opencv/
*/
float Vision::getOrientation(){
	Mat image = imageFilter(cam->getLastFrame());

	vector<vector<Point>> contours;
	getContours(image, contours);

	if (contours.size() == 0){
		visible = false;
		return 0;
	}
	visible = true;

	vector<Point> vectors;
	int numPoints = 0;
	for (int i = 0; i < contours.size(); i++)
		numPoints += contours[i].size();

	vectors.reserve(numPoints);
	for (int i = 0; i < contours.size(); i++)
		vectors.insert(vectors.end(), contours[i].begin(), contours[i].end());

	RotatedRect r = minAreaRect(vectors);
	Point2f recPoints[4];
	r.points(recPoints);

	//Draw rect
	for (int i = 0; i < 4; i++)
		line(processedImage, recPoints[i], recPoints[(i + 1) % 4], Scalar(0, 255, 0));

	//return angle
	if (r.size.width < r.size.height)
		return r.angle + 90;
	else
		return r.angle;
}

//Update vision target to account for changes due to lighting
void Vision::adaptiveFilter(Point2f center, float radius){
	Mat image = cam->getLastFrame();
	cvtColor(image, image, CV_BGR2HSV, 0);//convert image to HSV
	float sampleFactor = .5;//How much of target do we want to sample
	Rect imageSize(cv::Point(), image.size());
	this->resetHSVBounds();
	
	for (int i = -radius*sampleFactor; i < radius*sampleFactor; i++){
		for (int j = -radius*sampleFactor; j < radius*sampleFactor; j++){
			if (sqrt(i*i + j*j) < radius*sampleFactor){//Only sample pixels within circle
				Point p = Point(center.x, center.y) + Point(i,j);
				if (imageSize.contains(p)){
					processedImage.at<Vec3b>(p) = Vec3b(1, 1, 1);
					Scalar color = image.at<Vec3b>(p);
					if (color[0] < lowerHSVBound[0])
						lowerHSVBound[0] = color[0];
					else if (color[0] > upperHSVBound[0])
						upperHSVBound[0] = color[0];

					if (color[1] < lowerHSVBound[1])
						lowerHSVBound[1] = color[1];
					else if (color[1] > upperHSVBound[1])
						upperHSVBound[1] = color[1];

					if (color[2] < lowerHSVBound[2])
						lowerHSVBound[2] = color[2];
					else if (color[2] > upperHSVBound[2])
						upperHSVBound[2] = color[2];
				}
			}
		}
	}
}

Mat Vision::getLastFrame(){
	return processedImage.size().area() > 0 ? processedImage : cam->getLastFrame();
}

void Vision::update(){
	//handle keyboard input
	char c = key->lastChar();
	processedImage = cam->getLastFrame();
	switch (c){
		case 'm':
			cycleMode();
			break;
		case '.':
			increaseMinContour();
			break;
		case ',':
			decreaseMinContour();
			break;
		case '0':
			time_t now = time(0);
			struct tm  tstruct;
			char       buf[80];
			tstruct = *localtime(&now);
			strftime(buf, sizeof(buf), "%Y-%m-%d %H.%M.%S", &tstruct);
			string fileName  = "screenshot_" + string(buf) + ".jpg";
			try{
				imwrite("../screenshots/"+fileName, cam->getLastFrame());
			}
			catch (Exception e){
				MessageDisplay::add(MessageType::message, "Error saving screenshot: " + e.err);
			}
			break;
	}

	if (visible)
		MessageDisplay::add(MessageType::status, "Target Visible: Yes");
	else
		MessageDisplay::add(MessageType::status, "Target Visible: No");

	MessageDisplay::add(MessageType::status, "Target Radius: " + RUtility::floatToString(targetRadius));
}