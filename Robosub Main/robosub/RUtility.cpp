#include "RUtility.h"

string RUtility::floatToString(float x){
	char s[8];
	sprintf(s, "%3.2f", x);
	return string(s);
}