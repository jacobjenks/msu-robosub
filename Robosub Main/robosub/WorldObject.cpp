#pragma once

#include "WorldObject.h"

WorldObject::WorldObject(){

}

WorldObject::WorldObject(float inWidth, float inHeight) {
	this->width = inWidth;
	this->height = inHeight;
}

void WorldObject::setLocation(Point3f loc) {
	this->location = loc;
}

Point3f WorldObject::getLocation() {
	return this->location; //Need to handle a case where location is null
}
