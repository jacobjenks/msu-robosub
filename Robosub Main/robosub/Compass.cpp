#include "Compass.h"

Compass::Compass(char *portName) : SerialSensor(portName){
	key = unique_ptr<KeyboardListener>(new KeyboardListener());
	MessageDisplay::add(MessageType::message, "Initial Heading: "+ RUtility::floatToString(getData()));
}

void Compass::update(){
	if (!isConnected())
		return;

	int thresh = 5;

	if (key->lastChar() == '/'){
		calibrate();
	}
	else{
		char serialData[256] = "";
		WriteData("1", 1);
		ReadData(serialData, 256);
		currentHeading = atof(serialData);
	}
}

float Compass::getData(){
	return currentHeading;
}

void Compass::calibrate(){
	WriteData("c", 1);
}