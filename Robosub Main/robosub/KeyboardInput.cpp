#include "KeyboardInput.h"
#include "KeyboardListener.h"
#include <iostream>


KeyboardInput::KeyboardInput(){}

int KeyboardInput::attach(shared_ptr<KeyboardListener> k){
	views.push_back(k);
	return views.size() - 1;
}

void KeyboardInput::detach(int i){ views.erase(views.begin() + i); }

void KeyboardInput::notify(){
	for (int i = 0; i < views.size(); i++)
		views[i]->update(val);
}

void KeyboardInput::setVal(char c){ 
	val = c; 
	notify();
}