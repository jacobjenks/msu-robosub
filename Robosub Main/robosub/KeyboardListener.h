#pragma once;

class KeyboardListener{
private:
	char mlastChar;
	int index;
public:
	KeyboardListener();
	~KeyboardListener();
	void update(char c){ mlastChar = c; };
	char lastChar();
};