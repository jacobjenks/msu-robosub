#pragma once
#include "Navigation.h"
#include <memory>
#include "Updateable.h"
#include <opencv/cv.h>
#include "MessageDisplay.h"

using namespace cv;
using namespace std;

//Interface for controlling Navigation
class NavigationController{
	public:
		virtual bool update(Point3f) = 0;//returns whether or not the controller updated motor status
};