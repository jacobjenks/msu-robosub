#pragma once

#include "SerialSensor.h"
#include "KeyboardListener.h"


class Compass : SerialSensor{
private:
	float currentHeading;
	unique_ptr<KeyboardListener> key;

public:
	Compass(char *portName);

	float getData();
	void update();
	void calibrate();
};