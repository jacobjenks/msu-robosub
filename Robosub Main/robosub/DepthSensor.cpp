#include "DepthSensor.h"

DepthSensor::DepthSensor(char *portName) : SerialSensor(portName){
	surfacePSI = sensorToPSI(getDepthSensor());

	//Give it another 5 seconds if the first reading is bad
	clock_t start = clock();
	while (surfacePSI < 0 && ((clock() - start) < 5000))
		surfacePSI = sensorToPSI(getDepthSensor());

	//Manually set surface PSI for now to make testing easier.
	//Bozeman surface PSI: 14.4?
	surfacePSI = 17.2f;
	currentDepth = 0;
	MessageDisplay::add(MessageType::message, "Surface PSI: " + to_string(surfacePSI));
}

float DepthSensor::getDepthSensor(){
	if (!isConnected())
		return 0;

	char serialData[256] = "";
	WriteData("1", 1);
	ReadData(serialData, 256);
	return atof(serialData);
}

float DepthSensor::getDepthFeet(){
	return sensorToDepth(getDepthSensor());
}

/*
I arrived at this conversion based on the valToPsi function in the PressureSensor class of the old code.
The comments from that code are:
1024/5 convert to volts
Subtract one volt because pressure sensor is 1-5v
50PSI/4volts Final PSI
I honestly have no idea what those comments are talking about, but they appear to produce reasonable
values (14.35 PSI from my first measurement in the lab)
*/
float DepthSensor::sensorToPSI(float val){
	return ((val * .0048828125) - 1)*12.5;
}

//same as above, but returns depth in feet
float DepthSensor::sensorToDepth(float val){
	return (sensorToPSI(val) - surfacePSI) / 0.433;
}

void DepthSensor::update(){
	currentDepth = getDepthFeet();
}

float DepthSensor::getData(){
	return currentDepth;
}