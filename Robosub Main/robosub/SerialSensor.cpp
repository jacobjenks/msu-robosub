#include "SerialSensor.h"

SerialSensor::SerialSensor(char *portName)
{
	//We're not yet connected
	this->connected = false;
	
#ifdef _WIN32
	//Try to connect to the given port throuh CreateFile
	this->hSerial = CreateFileA(portName,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
#endif

#ifdef __linux__
	this->hSerial = open(portName, O_RDWR | O_NOCTTY | O_NDELAY);
#endif
	//Check if the connection was successfull
	if (this->hSerial == INVALID_HANDLE_VALUE)
	{
		
		//If not success full display an Error
#ifdef _WIN32
		if (GetLastError() == ERROR_FILE_NOT_FOUND)
#endif
#ifdef __linux__
		if (1)	
#endif
		{
			//Print Error if neccessary
			MessageDisplay::add(MessageType::message, "Could not connect to Arduino: " + string(portName) + " not available.");
		}
		else
			MessageDisplay::add(MessageType::message, "Could not connect to Arduino: unknown error.");
	}
	else
	{
#ifdef _WIN32
		//If connected we try to set the comm parameters
		DCB dcbSerialParams = { 0 };

		//Try to get the current
		if (!GetCommState(this->hSerial, &dcbSerialParams))
#endif
#ifdef __linux__
		struct termios options;

		//Get current options
		if (tcgetattr(this->hSerial, &options) != 0)
#endif
		{
			//If impossible, show an error
			MessageDisplay::add(MessageType::message, "Failed to get current serial parameters!");
		}
		else
		{
#ifdef _WIN32
			//Define serial connection parameters for the arduino board
			dcbSerialParams.BaudRate = CBR_9600;
			dcbSerialParams.ByteSize = 8;
			dcbSerialParams.StopBits = ONESTOPBIT;
			dcbSerialParams.Parity = NOPARITY;
			//Setting the DTR to Control_Enable ensures that the Arduino is properly
			//reset upon establishing a connection
			dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;

			//Set the parameters and check for their proper application
			if (!SetCommState(hSerial, &dcbSerialParams))
#endif
#ifdef __linux__
			//Non-raw mode
			fcntl(this->hSerial, F_SETFL, FNDELAY);
			//Set baud rate
			cfsetispeed(&options, B9600);
			cfsetospeed(&options, B9600);

			//Set local and enable receiver
			options.c_cflag |= (CLOCAL | CREAD);

			//Set to 8N1 - Data bits | Parity | Stop bits
			options.c_cflag &= ~PARENB	//parity off
			options.c_cflag &= ~CSTOPB	//stop bits 1
			options.c_cflag &= ~CSIZE;	//data bits 8
			options.c_cflag |= CS8;		//data bits 8

			//Set local flags to Non-canonical, no echo, no echo-erase, no SIG signals
			options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

			//Raw output flag - Ensuring the bit is off
			options.c_oflag &= ~OPOST;

			if (tcsetattr(this->hSerial, TCSAFLUSH, &options) != 0)
#endif	
			{
				MessageDisplay::add(MessageType::message, "Could not set Serial Port parameters");
			}
			else
			{
				//If everything went fine we're connected
				this->connected = true;
#ifdef _WIN32
				//Flush any remaining characters in the buffers 
				PurgeComm(this->hSerial, PURGE_RXCLEAR | PURGE_TXCLEAR);
#endif
				//We wait 2s as the arduino board will be resetting
				std::this_thread::sleep_for(std::chrono::milliseconds(ARDUINO_WAIT_TIME));
			}
		}
	}
}

SerialSensor::~SerialSensor()
{
	//Check if we are connected before trying to disconnect
	if (this->connected)
	{
		//We're no longer connected
		this->connected = false;
		//Close the serial handler
#ifdef _WIN32
		CloseHandle(this->hSerial);
#endif
#ifdef __linux__
		close(this->hSerial);
#endif
	}
}

int SerialSensor::ReadData(char *buffer, unsigned int nbChar)
{
	//Number of bytes we'll have read
	unsigned long bytesRead;
	//Number of bytes we'll really ask to read
	unsigned int toRead;

#ifdef _WIN32
	//Use the ClearCommError function to get status info on the Serial port
	ClearCommError(this->hSerial, &this->errors, &this->status);

	//Check if there is something to read
	if (this->status.cbInQue > 0)
#endif
#ifdef __linux__
	unsigned int availableBytes = ioctl(this->hSerial, FIONREAD);
	if (avialableBytes > 0)
#endif
		
	{
		//If there is we check if there is enough data to read the required number
		//of characters, if not we'll read only the available characters to prevent
		//locking of the application.
#ifdef _WIN32
		if (this->status.cbInQue>nbChar)
#endif
#ifdef __linux__
		if (availableBytes > nbChar)
#endif
		{
			toRead = nbChar;
		}
		else
		{
#ifdef _WIN32
			toRead = this->status.cbInQue;
#endif
#ifdef __linux__
			toRead = availableBytes;
#endif
		}

		//Try to read the require number of chars, and return the number of read bytes on success
#ifdef _WIN32
		if (ReadFile(this->hSerial, buffer, toRead, &bytesRead, NULL) && bytesRead != 0)
#endif 
#ifdef __linux__
		if (read(this->hSerial, buffer, toRead) != -1)
#endif
		{
			return bytesRead;
		}

	}

	//If nothing has been read, or that an error was detected return -1
	return -1;

}


bool SerialSensor::WriteData(char *buffer, unsigned int nbChar)
{
	unsigned long bytesSend;

	//Try to write the buffer on the Serial port
#ifdef _WIN32
	if (!WriteFile(this->hSerial, (void *)buffer, nbChar, &bytesSend, 0))
#endif
#ifdef __linux__
	if (write(this->hSerial, (void *)buffer, nbChar) != nbChar)
#endif
	{
		//In case it don't work get comm error and return false
#ifdef _WIN32
		ClearCommError(this->hSerial, &this->errors, &this->status);
#endif
		return false;
	}
	else
		return true;
}

bool SerialSensor::isConnected()
{
	//Simply return the connection status
	return this->connected;
}