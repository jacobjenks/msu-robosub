
#include "Camera.h"

using namespace std;

Camera::Camera(const string name, const int camNumber){
	this->valid = true;
	this->name = name;
	this->video = VideoCapture(camNumber);
	if (!this->video.isOpened()){
		this->valid = false;
		MessageDisplay::add(MessageType::message, "Camera " + to_string(camNumber) + " failed to initialize");
	}
	k = unique_ptr<KeyboardListener>(new KeyboardListener());
	paused = false;
}

void Camera::update(){
	switch (k->lastChar()){
		case '\\':
			paused = !paused;
			break;
	}

	if (valid && !paused)
		video.read(lastFrame);
	else if (!paused)
		lastFrame = imread("../Screenshots/screenshot_2015-07-21 16.33.50.jpg", CV_LOAD_IMAGE_COLOR);
		//lastFrame = imread("default.jpg", CV_LOAD_IMAGE_COLOR);
}

/*
	Return a clone of the last frame.
*/
Mat Camera::getLastFrame(){
	return lastFrame.clone();
}