#pragma once

#include <string>
#define NaN 1.0000000000000001e+300
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

class RUtility{
	public:
		static string floatToString(float x);
};