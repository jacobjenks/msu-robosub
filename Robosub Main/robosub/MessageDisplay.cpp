#include "MessageDisplay.h"

clock_t MessageDisplay::start = clock();
vector<string> MessageDisplay::status;
vector<string> MessageDisplay::message;
vector<string> MessageDisplay::output;
int MessageDisplay::currentMessage = 0;
string MessageDisplay::logFileName = "";



void MessageDisplay::add(MessageType t, string s){
	clock_t elapsed = clock() - start;
	char ct[10];
	sprintf(ct, "%2.2f", ((float)elapsed / CLOCKS_PER_SEC));
	string timestamp = "[" + string(ct) + "]: ";

	//output.push_back(timestamp + s);

	switch (t){
		case MessageType::message:{
			message.push_back(timestamp + s);
			currentMessage++;
			break;
		}
		case MessageType::status:
			status.push_back(s);
			break;
	}

	printToFile();
}

//add messages to image and then delete messages
void MessageDisplay::dump(Mat image){

	//print status
	for (int i = 0; i < status.size(); i++){
		putText(image,
			status[i],
			Point(5, (image.size().height - (status.size() * fontLineSize) + (i*fontLineSize))),
			CV_FONT_HERSHEY_PLAIN,
			1,
			Scalar(255, 255, 255),
			1,
			CV_AA);
	}

	//print log
	for (int i = (currentMessage > numMessages ? currentMessage - numMessages : 0); i < currentMessage; i++){
		putText(image,
			message[i],
			Point(5, fontLineSize + ((i - (currentMessage > numMessages ? currentMessage - numMessages : 0))*fontLineSize)),
			CV_FONT_HERSHEY_PLAIN,
			1,
			Scalar(255, 255, 255),
			1,
			CV_AA);
	}

	//print motor status
	vector<string> motorStatus = Navigation::getNav().getMotorStatus();
	for (int i = 0; i < motorStatus.size(); i++){
		putText(image,
			motorStatus[i],
			Point(image.size().width - 140, (image.size().height - (motorStatus.size() * fontLineSize) + (i*fontLineSize))),
			CV_FONT_HERSHEY_PLAIN,
			1,
			Scalar(255, 255, 255),
			1,
			CV_AA);
	}

	status.clear();
}

int MessageDisplay::messageSize(){
	return message.size();
}

void MessageDisplay::printToFile(){
	ofstream file;

	//create file name
	if (logFileName == ""){
		time_t now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d %H.%M.%S", &tstruct);
		logFileName = "log_"+string(buf);
	}

	//create directory if necessary
	struct stat info;
	if ((stat("../log", &info) != 0) || !(info.st_mode & S_IFDIR))
		_mkdir("../log");
	

	file.open("../log/"+logFileName+".txt");
	if (!file.is_open())
		cout << "Failed to open log file." << "\r\n";
	for (int i = 0; i < output.size(); i++)
		file << output.at(i) << "\n";
	file.close();
}