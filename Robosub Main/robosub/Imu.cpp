#include "phidget21.h"
#include "Imu.h"
#include <iostream>
#include <atomic>
#include <mutex>

using namespace Imu;

int __stdcall SpatialDataEventHandler(CPhidgetSpatialHandle phid, void *userPtr, CPhidgetSpatial_SpatialEventDataHandle *data, int dataCount);
int __stdcall AttachHandler(CPhidgetHandle phid, void *userData);

std::mutex ImuController::mutex;

ImuController::ImuController(){
	int serialNo;
	int ret;

	this->data = (CPhidgetSpatial_SpatialEventData*)malloc(sizeof(CPhidgetSpatial_SpatialEventData) * 1);
        //Create spatial handle(imu product))
	ret = CPhidgetSpatial_create(&phid);
        
        //Open handle
	ret = CPhidget_open((CPhidgetHandle)phid, -1);
        
	// Necessary to connect to device, uses onattach callback to proceed
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)phid, AttachHandler, (this->data));
	//CPhidgetSpatial_setCompassCorrectionParameters((CPhidgetSpatialHandle)phid, 0.32375, -0.10657, 0.12950, 0.00000, 3.03255, 3.14514, 3.08885, 0.00018, 0.00000, 0.00017, 0.00000, 0.00000, 0.00000);
        
        //This shouldn't be here, but probably won't cause issues
	if ((ret = CPhidget_waitForAttachment((CPhidgetHandle)phid, 500))) {
                
		MessageDisplay::add(MessageType::message, "Failed to initialize IMU");
	}
        CPhidgetSpatial_zeroGyro(phid);
	ret = CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);

	//initialize IMU readings
	lastMag[0] = 0;
	lastMag[1] = 0;
	lastMag[2] = 0;

	lastAccel[0] = 0;
	lastAccel[1] = 0;
	lastAccel[2] = 0;

	lastGyro[0] = 0;
	lastGyro[1] = 0;
	lastGyro[2] = 0;

	pitch = roll = yaw = 0;
}

double* ImuController::getMagneticReading() {
	/*
	double result;
	CPhidgetSpatial_getMagneticField(phid, 0, &result);
	return result;
	*/
	ImuController::mutex.lock();
	double* mag = this->data->magneticField;
	ImuController::mutex.unlock();
	return mag;
}

double* ImuController::getAccelData() {
	ImuController::mutex.lock();
	double* accel = this->data->acceleration;
	ImuController::mutex.unlock();
	return accel;
}

double* ImuController::getGyroData() {
	ImuController::mutex.lock();
	double* gyro = this->data->angularRate;
	ImuController::mutex.unlock();
	return gyro;
}

void ImuController::update(){
	double *mag = this->getMagneticReading();
	if (mag[0] != NaN){
		lastMag[0] = mag[0];
		lastMag[1] = mag[1];
		lastMag[2] = mag[2];
	}

	double *accel = this->getAccelData();
	if (accel[0] != NaN){
		lastAccel[0] = accel[0];
		lastAccel[1] = accel[1];
		lastAccel[2] = accel[2];
	}

	double *gyro = this->getGyroData();
	if (gyro[0] != NaN){
		lastGyro[0] = gyro[0];
		lastGyro[1] = gyro[1];
		lastGyro[2] = gyro[2];
	}

	this->computeAngles();
}

//Compute pitch, roll, and yaw, taken from this page http://www.phidgets.com/docs/Compass_Primer
//Values should theoretically range from 0-360. This doesn't appear to be the case in practice
void ImuController::computeAngles(){
	double rollAngle = std::atan2(lastAccel[1], lastAccel[2]);
	double pitchAngle = std::atan(-lastAccel[0] / ((lastAccel[1] * std::sin(rollAngle)) + (lastAccel[2] * std::cos(rollAngle))));
	double yawAngle = std::atan2(
		(lastMag[2] * std::sin(rollAngle)) - (lastMag[1] * std::cos(rollAngle)),
		(lastMag[0] * std::cos(pitchAngle)) + (lastMag[1] * std::sin(pitchAngle) * std::sin(rollAngle)) + (lastMag[2] * std::sin(pitchAngle) * std::cos(rollAngle))
	);

	//Convert radians to degrees
	yaw = (yawAngle * (180 / M_PI))+180;
	pitch = (pitchAngle * (180 / M_PI))+180;
	roll = (rollAngle * (180 / M_PI))+180;
}


int __stdcall AttachHandler(CPhidgetHandle phid, void *userData) {
    CPhidgetSpatial_zeroGyro((CPhidgetSpatialHandle)phid);
    CPhidgetSpatial_set_OnSpatialData_Handler ((CPhidgetSpatialHandle)phid, SpatialDataEventHandler, userData);
    return 0;
}

int __stdcall SpatialDataEventHandler(CPhidgetSpatialHandle phid, void *userPtr, CPhidgetSpatial_SpatialEventDataHandle *data, int dataCount) {
	ImuController::mutex.lock();
	memcpy(userPtr, *data, sizeof(CPhidgetSpatial_SpatialEventData));
	ImuController::mutex.unlock();
    return 0;
}