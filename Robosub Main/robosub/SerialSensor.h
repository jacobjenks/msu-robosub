#ifndef SERIALCLASS_H_INCLUDED
#define SERIALCLASS_H_INCLUDED

#define ARDUINO_WAIT_TIME 2000

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __linux__
#include <termios.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include "MessageDisplay.h"
#include "RUtility.h"

class SerialSensor
{
private:
	//Serial comm handler
	void* hSerial;
	//Connection status
	bool connected;

	#ifdef _WIN32
	//Get various information about the connection
	COMSTAT status;
	#endif

	//Keep track of last error
	unsigned long errors;
public:
	//Initialize Serial communication with the given COM port
	SerialSensor(char *portName);
	//Close the connection
	~SerialSensor();
	//Read data in a buffer, if nbChar is greater than the
	//maximum number of bytes available, it will return only the
	//bytes available. The function return -1 when nothing could
	//be read, the number of bytes actually read.
	int ReadData(char *buffer, unsigned int nbChar);
	//Writes data from a buffer through the Serial connection
	//return true on success.
	bool WriteData(char *buffer, unsigned int nbChar);
	//Check if we are actually connected
	bool isConnected();

	virtual void update() = 0;//Refresh sensor data
	virtual float getData() = 0;//Get data from sensor
};

#endif // SERIALCLASS_H_INCLUDED