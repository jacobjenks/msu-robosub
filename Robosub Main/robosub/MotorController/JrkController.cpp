/* 
 * File:   JrkController.cpp
 * Author: arbiter34
 * 
 * Created on February 6, 2015, 2:10 PM
 */

#include "JrkController.hpp"
#include "Exception.hpp"
#include "protocol.h"
#include <sstream>

using namespace Jrk;

std::string JrkController::productName = "Pololu Jrk";
std::string JrkController::shortProductName = "Jrk";

/// <summary>
/// The first part of the device instance ID for the bootloader for the Jrk 21v3.
/// </summary>
std::string JrkController::bootloaderDeviceInstanceIdPrefix82 = "USB\\VID_1FFB&PID_0082";

/// <summary>
/// The first part of the device instance ID for the bootloader for the next Jrk.
/// </summary>
std::string JrkController::bootloaderDeviceInstanceIdPrefix84 = "USB\\VID_1FFB&PID_0084";

std::ostringstream os;

JrkController::JrkController(UsbWrapper::DeviceListItem *deviceListItem)
	: UsbWrapper::UsbDevice(deviceListItem)
{
	switch (getProductID())
	{
		case 0x0083:
			this->product = jrkProduct::umc01a;
			break;
		case 0x0085:
			this->product = jrkProduct::umc02a;
			break;
		default:
			this->product = jrkProduct::UNKNOWN;
			break;
	}
}

/// <summary>
/// True if to get a current reading we need to divide the current variable by the duty cycle.
/// </summary>
bool JrkController::divideCurrent()
{
	return product == umc01a;
}

/// <summary>
/// See Sec 16.3 of the PIC18F14K50 datasheet for information about SPBRG.
/// On the umc01a, we have SYNC=0, BRG16=1, and BRGH=1, so the pure math
/// formula for the baud rate is Baud = INSTRUCTION_FREQUENCY / (spbrg+1);
/// </summary>
uint32_t JrkController::convertSpbrgToBps(uint16_t spbrg)
{
	if (spbrg == 0)
	{
		return 0;
	}

	return (uint32_t)((INSTRUCTION_FREQUENCY + (spbrg + 1) / 2) / (spbrg + 1));
}

/// <summary>
/// The converts from bps to SPBRG, so it is the opposite of convertSpbrgToBps.
/// The purse math formula is spbrg = INSTRUCTION_FREQUENCY/Baud - 1.
/// </summary>
/// <param name="value"></param>
/// <returns></returns>
uint16_t JrkController::convertBpsToSpbrg(uint32_t bps)
{
	if (bps == 0)
	{
		return 0;
	}

	return (uint16_t)((INSTRUCTION_FREQUENCY - bps/2) / bps);
}

void JrkController::testBaudRateConversions()
{
	// These numbers are taken from Table 16-5, with SYNC=0, BRG16=1, BRGH=1, FOSC=48MHZ.
	assertBaudConversion(39999, 300);
	assertBaudConversion(9999, 1200);
	assertBaudConversion(4999, 2400);
	assertBaudConversion(1249, 9600);
	assertBaudConversion(1151, 10417);
	assertBaudConversion(624, 19200);
	assertBaudConversion(207, 57600);
	assertBaudConversion(103, 115200);
	
	// 184 is the minimum value the user can enter for the baud rate
	assertBaudConversion(65216, 184);

	// 115385 is the maximum value the user can enter for the baud rate
	assertBaudConversion(103, 115385);
}

void JrkController::assertBaudConversion(uint16_t spbrg, uint32_t bps)
{
    // If the user types this baud rate in, we want SPBRG to be correct.
    
    os << bps << " bps";
    assertEqual(spbrg, convertBpsToSpbrg(bps), os.str());

    // If the user then loads the baud rate in to the configuration utility,
    // he will see a different BPS value but that is okay, as long as SPBRG
    // remains the same when he clicks Apply.
    uint32_t modifiedBps = convertSpbrgToBps(spbrg);
    
    os << modifiedBps << " bps should produce the same spbrg as " << bps << " bps.";
    assertEqual(spbrg, convertBpsToSpbrg(modifiedBps), os.str());
}

void JrkController::assertEqual(int32_t expected, int32_t actual, std::string description)
{
	if (expected != actual)
	{
		
		os << description << "  Expected " << expected << " but got " << actual << ".";
		throw new Exception(os.str());
	}
}

void JrkController::setTarget(uint16_t target)
{
	requireArgumentRange(target, 0, 4095, "target");

	try
	{            
            controlTransfer(0x40, (uint8_t)jrkRequest::REQUEST_SET_TARGET, target, 0);
	}
	catch (Exception& exception)
	{	
		(void)exception;			//This is to supress the compiler warning
            throw new Exception("There was an error setting the target.");
	}
}

void JrkController::motorOff()
{
	try
	{
		controlTransfer(0x40, (uint8_t)jrkRequest::REQUEST_MOTOR_OFF, 0, 0);
	}
	catch (Exception& exception)
	{
		(void)exception;		//Suppress compiler warning about unused local var
		throw new Exception("There was an error turning off the motor.");
	}
}

/// <summary>
/// Gets the jrk parameter.  The only one that is modified by this function relative to
/// what is actually on the device is the serial baud rate.
/// </summary>
/// <param name="parameterId"></param>
/// <returns></returns>
uint32_t JrkController::getJrkParameter(jrkParameter parameterId)
{
	uint32_t value = getParameter(parameterId);
	if (parameterId == jrkParameter::PARAMETER_SERIAL_FIXED_BAUD_RATE)
	{
		return convertSpbrgToBps((uint16_t)value);
	}
	
	return value;
}

uint32_t JrkController::getParameter(jrkParameter parameterId)
{
	Range *range = JrkExtensionMethods::range(parameterId);

	try
	{
		return getRequest(jrkRequest::REQUEST_GET_PARAMETER, (uint16_t)parameterId, range->uint8_ts);
	}
	catch (Exception exception)
	{
		
		os << "There was an error reading " << parameterId << " from the device.";
		throw new Exception(os.str());
	}                    
}

/// <summary>
/// Clears the error flag bits so that the motor can run again.
/// Does not clear the "waiting for command" bit.
/// </summary>
void JrkController::clearErrors()
{
	try
	{
		controlTransfer(0x40, (uint8_t)jrkRequest::REQUEST_CLEAR_ERRORS, 0, 0);
	}
	catch (Exception exception)
	{
		throw new Exception("There was an error clearing the device's error bits.");
	}
}

/// <summary>
/// sets the parameter on the device.  The only parameter modified before setting is the
/// serial fixed baud rate, which is converted to the PIC's SPBRG setting.
/// </summary>
/// <param name="parameterId"></param>
/// <param name="value"></param>
void JrkController::setJrkParameter(jrkParameter parameterId, uint32_t value)
{
	if (parameterId == jrkParameter::PARAMETER_SERIAL_FIXED_BAUD_RATE)
	{
		value = convertBpsToSpbrg(value);
	}
	setParameter(parameterId, value);
}

void JrkController::setParameter(jrkParameter parameterId, uint32_t value)
{
	Range *range = JrkExtensionMethods::range(parameterId);
        os << parameterId;
	requireArgumentRange(value, range->minimumValue, range->maximumValue, os.str());

	try
	{
		if (range->uint8_ts == 1)
		{
			setRequestU8(jrkRequest::REQUEST_SET_PARAMETER, (uint8_t)parameterId, (uint8_t)value);
		}
		else if (range->uint8_ts == 2)
		{
			setRequestU16(jrkRequest::REQUEST_SET_PARAMETER, (uint8_t)parameterId, (uint16_t)value);
		}
		else
		{
			
			os << "uint8_t count " << range->uint8_ts << " is not implemented yet.";
			throw new Exception(os.str());
		}
	}
	catch (Exception exception)
	{
		
		os << "There was an error setting " << parameterId << ".";
		throw new Exception(os.str());
	}
}

void JrkController::setRequestU8(jrkRequest requestId, uint8_t id, uint8_t value)
{
	controlTransfer(0x40, (uint8_t)requestId, value, (uint16_t)(id + (1 << 8)));
}

void JrkController::setRequestU16(jrkRequest requestId, uint8_t id, uint16_t value)
{
	controlTransfer(0x40, (uint8_t)requestId, value, (uint16_t)(id + (2 << 8)));
}

void JrkController::startBootloader()
{
	try
	{
		controlTransfer(0x40, (uint8_t)jrkRequest::REQUEST_START_BOOTLOADER, 0, 0);
	}
	catch (Exception exception)
	{
		throw new Exception("There was an error starting the bootloader.  The firmware on this device may be corrupted.");
	}
}

/// <summary>
/// Reinitializes the motor controller system.
/// </summary>
void JrkController::reinitialize()
{
	try
	{
		controlTransfer(0x40, (uint8_t)jrkRequest::REQUEST_REINITIALIZE, 0, 0);
	}
	catch (Exception exception)
	{
		throw new Exception("There was an error re-initializing the device.");
	}
}

void JrkController::requireArgumentRange(uint32_t argumentValue, int32_t minimum, int32_t maximum, std::string argumentName)
{
	if (argumentValue < minimum || argumentValue > maximum)
	{
		
		os << "The " << argumentName << " must be between " << minimum <<
			" and " << maximum << ", but the value given was " << argumentValue;
		throw new Exception(os.str());
	}
}

uint16_t JrkController::getRequest(jrkRequest requestId, uint16_t id, uint8_t length)
{
	uint16_t value = 0;

	unsigned char *value_array;

	if (length == 2)
	{
            value_array = (unsigned char*)malloc(sizeof(unsigned char)*2);    
            controlTransfer(0xC0, (uint8_t)requestId, 0, (uint16_t)id, value_array, (int32_t)2);
            value = (uint16_t)(value_array[0] + 256 * value_array[1]);
	}
	else if (length == 1)
	{
            value_array = (unsigned char*)malloc(sizeof(unsigned char)*1);    
            controlTransfer(0xC0, (uint8_t)requestId, 0, (uint16_t)id, value_array, (int32_t)1);
            value = value_array[0];
	}
	else
	{
		throw new Exception("length must be 1 or 2");
	}

	return value;
}

jrkVariables* JrkController::getVariables()
{
	unsigned char *array = (unsigned char*)malloc(sizeof(unsigned char) * sizeof(jrkVariables));
	uint32_t lengthTransferred = controlTransfer(0xC0, (uint8_t)jrkRequest::REQUEST_GET_VARIABLES, 0, 0, array, sizeof(jrkVariables));

	if (lengthTransferred != sizeof(jrkVariables))
	{
		
		os << "Error getting variables from Jrk.  Expected " << sizeof(jrkVariables) << " uint8_ts, received " << lengthTransferred << ".";
		throw new Exception(os.str());
	}

	jrkVariables *variables = (jrkVariables*)malloc(sizeof(jrkVariables)*1);
	variables = (jrkVariables*)array;
	

	return variables;
}


uint16_t JrkController::getFirmwareVersionMajor()
{
	if (this->privateFirmwareVersionMajor == 0xFFFF)
	{
		getFirmwareVersion();
	}
	return this->privateFirmwareVersionMajor;
}

uint8_t JrkController::getFirmwareVersionMinor()
{
	if (this->privateFirmwareVersionMajor == 0xFFFF)
	{
		getFirmwareVersion();
	}
	return this->privateFirmwareVersionMinor;
}

std::string JrkController::getFirmwareVersionString()
{
	if (this->privateFirmwareVersionMajor == 0xFFFF)
	{
		getFirmwareVersion();
	}
	os << (int)this->privateFirmwareVersionMajor << "." << (int)this->privateFirmwareVersionMinor;
        std::string test = os.str();
	return os.str();
}

void JrkController::getFirmwareVersion()
{
	uint8_t buffer[14];
	
	try
	{
		controlTransfer(0x80, 6, 0x0100, 0x0000, buffer, 14);
	}
	catch(Exception exception)
	{
		throw new Exception("There was an error getting the firmware version from the device.");
	}

	this->privateFirmwareVersionMinor = (uint8_t)((buffer[12] & 0xF));
	this->privateFirmwareVersionMajor = (uint16_t)((buffer[12] >> 4 & 0xF) + (buffer[13] & 0xF)*10 + (buffer[13] >> 4 & 0xF)*100);
        
}

std::vector<UsbWrapper::DeviceListItem*> JrkController::getConnectedDevices()
{
	// try
	// {
		// return UsbDevice.getDeviceList(Jrk.deviceInterfaceGuid);
	// }
	// catch(Exception& exception)
	// {
    uint16_t productIds[2] = {0x0083, 0x0085};    
    return UsbWrapper::UsbDevice::getDeviceList(0x1ffb, productIds, 2);
	// }
}

Range* Range::u16 = new Range(2, 0, 0xFFFF);
Range* Range::u12 = new Range(2, 0, 0x0FFF);
Range* Range::u10 = new Range(2, 0, 0x03FF);
Range* Range::u8 = new Range(1, 0, 0xFF);
Range* Range::boolean = new Range(1, 0, 1);

Range::Range(uint8_t uint8_ts, int32_t minimumValue, int32_t maximumValue)
{
	this->uint8_ts = uint8_ts;
	this->minimumValue = minimumValue;
	this->maximumValue = maximumValue;
}

bool Range::isSigned()
{
	return this->minimumValue < 0;            
}



Range* JrkExtensionMethods::range(jrkParameter parameterId)
{
	switch (parameterId)
	{
		default:
                    os << "Invalid parameterId " << parameterId << ", can not determine the range of this parameter.";
                    Exception(os.str());

		case jrkParameter::PARAMETER_INITIALIZED: return new Range(1, 0, 255);
		case jrkParameter::PARAMETER_INPUT_MODE: return new Range(1,0,2); // enum
            case jrkParameter::PARAMETER_INPUT_MINIMUM: return Range::u12;
		case jrkParameter::PARAMETER_INPUT_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_OUTPUT_MINIMUM: return Range::u12;
		case jrkParameter::PARAMETER_OUTPUT_NEUTRAL: return Range::u12;
		case jrkParameter::PARAMETER_OUTPUT_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_INPUT_INVERT: return Range::boolean;
		case jrkParameter::PARAMETER_INPUT_SCALING_DEGREE: return Range::u8;  // But anything over 4 is crazy
		case jrkParameter::PARAMETER_INPUT_POWER_WITH_AUX: return Range::boolean;
		case jrkParameter::PARAMETER_FEEDBACK_DEAD_ZONE: return Range::u8;
		case jrkParameter::PARAMETER_INPUT_ANALOG_SAMPLES_EXPONENT: return new Range(1,0,8);
		case jrkParameter::PARAMETER_INPUT_DISCONNECT_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_INPUT_DISCONNECT_MINIMUM: return Range::u12;
		case jrkParameter::PARAMETER_INPUT_NEUTRAL_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_INPUT_NEUTRAL_MINIMUM: return Range::u12;

		case jrkParameter::PARAMETER_SERIAL_MODE: return new Range(1, 0, 3);
		case jrkParameter::PARAMETER_SERIAL_FIXED_BAUD_RATE: return Range::u16;
		case jrkParameter::PARAMETER_SERIAL_TIMEOUT: return Range::u16;
		case jrkParameter::PARAMETER_SERIAL_ENABLE_CRC: return Range::boolean;
		case jrkParameter::PARAMETER_SERIAL_NEVER_SUSPEND: return Range::boolean;
		case jrkParameter::PARAMETER_SERIAL_DEVICE_NUMBER: return new Range(1, 0, 127);

		case jrkParameter::PARAMETER_FEEDBACK_MODE: return new Range(1,0,5);  // enum
		case jrkParameter::PARAMETER_FEEDBACK_MINIMUM: return Range::u12;
		case jrkParameter::PARAMETER_FEEDBACK_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_FEEDBACK_INVERT: return Range::boolean;
		case jrkParameter::PARAMETER_FEEDBACK_POWER_WITH_AUX: return Range::boolean;
		case jrkParameter::PARAMETER_FEEDBACK_DISCONNECT_MAXIMUM: return Range::u12;
		case jrkParameter::PARAMETER_FEEDBACK_DISCONNECT_MINIMUM: return Range::u12;

		case jrkParameter::PARAMETER_FEEDBACK_ANALOG_SAMPLES_EXPONENT: return new Range(1,0,8);

		case jrkParameter::PARAMETER_PROPORTIONAL_MULTIPLIER: return Range::u10;
		case jrkParameter::PARAMETER_PROPORTIONAL_EXPONENT: return new Range(1,0,15);
		case jrkParameter::PARAMETER_INTEGRAL_MULTIPLIER: return Range::u10;
		case jrkParameter::PARAMETER_INTEGRAL_EXPONENT: return new Range(1,0,15);
		case jrkParameter::PARAMETER_DERIVATIVE_MULTIPLIER: return Range::u10;
		case jrkParameter::PARAMETER_DERIVATIVE_EXPONENT: return new Range(1,0,15);

		case jrkParameter::PARAMETER_PID_PERIOD: return new Range(2,1, 8191);
		case jrkParameter::PARAMETER_PID_INTEGRAL_LIMIT: return new Range(2, 0, 32767);
		case jrkParameter::PARAMETER_PID_RESET_INTEGRAL: return Range::boolean;
		
		case jrkParameter::PARAMETER_MOTOR_PWM_FREQUENCY: return new Range(1,0,2); // enum
		case jrkParameter::PARAMETER_MOTOR_INVERT: return Range::boolean;
		case jrkParameter::PARAMETER_MOTOR_MAX_ACCELERATION_FORWARD: return new Range(2,1,600);
		case jrkParameter::PARAMETER_MOTOR_MAX_ACCELERATION_REVERSE: return new Range(2,1,600);
		case jrkParameter::PARAMETER_MOTOR_MAX_DUTY_CYCLE_FORWARD: return new Range(2,0,600);
		case jrkParameter::PARAMETER_MOTOR_MAX_DUTY_CYCLE_REVERSE: return new Range(2,0,600);
		case jrkParameter::PARAMETER_MOTOR_MAX_CURRENT_FORWARD: return new Range(1,0,255);
		case jrkParameter::PARAMETER_MOTOR_MAX_CURRENT_REVERSE: return new Range(1,0, 255);
		case jrkParameter::PARAMETER_MOTOR_CURRENT_CALIBRATION_FORWARD: return new Range(1,0,255);
		case jrkParameter::PARAMETER_MOTOR_CURRENT_CALIBRATION_REVERSE: return new Range(1, 0, 255);
		case jrkParameter::PARAMETER_MOTOR_BRAKE_DURATION_FORWARD: return new Range(1,0,255);
		case jrkParameter::PARAMETER_MOTOR_BRAKE_DURATION_REVERSE: return new Range(1,0,255);
		case jrkParameter::PARAMETER_MOTOR_COAST_WHEN_OFF: return Range::boolean;
		case jrkParameter::PARAMETER_MOTOR_MAX_DUTY_CYCLE_WHILE_FEEDBACK_OUT_OF_RANGE: return new Range(2, 1, 600);
		
		case jrkParameter::PARAMETER_ERROR_ENABLE: return Range::u16;
		case jrkParameter::PARAMETER_ERROR_LATCH: return Range::u16;
	}
}



