// UsbWrapper_Linux/UsbDevice.cs
//   An interface between Pololu USB code and libusb-1.0 in Linux.

#include <vector>
#include <exception>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include "usbdevice.hpp"
#include "Exception.hpp"

using namespace UsbWrapper;
//{
       
            /// <summary>
            /// The text to display to the user in the list to represent this
            /// device.  By default, this text is "#" + serialNumberString,
            /// but it can be changed to suit the application's needs
            /// (for example, adding model information to it).
            /// </summary>
            std::string DeviceListItem::getText() { return this->privateText; }

            void DeviceListItem::setText(std::string text) { this->privateText = text; }


            /// <summary>
            /// Gets the serial number.
            /// </summary>
            std::string DeviceListItem::getSerialNumber() { return privateSerialNumber; }

            /// <summary>
            /// Gets the USB product ID of the device.
            /// </summary>
            uint16_t DeviceListItem::getProductID() { return this->privateProductId; }

            /// <summary>
            /// true if the devices are the same
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            bool DeviceListItem::isSameDeviceAs(DeviceListItem item) { return (this->privateDevicePointer == item.getDevicePointer()); }



            /// <summary>
            /// Creates an item that doesn't actually refer to a device; just for populating the list with things like "Disconnected"
            /// </summary>
            /// <param name="text"></param>
            DeviceListItem DeviceListItem::CreateDummyItem(std::string text) {
                DeviceListItem item = DeviceListItem(NULL,text,"",0);
                return item;
            }

            /// <summary>
            /// Gets the device pointer.
            /// </summary>
            libusb_device* DeviceListItem::getDevicePointer() { return this->privateDevicePointer; }



            DeviceListItem::DeviceListItem(libusb_device *devicePointer, std::string text, std::string serialNumber, uint16_t productId)
            {
                this->privateDevicePointer = devicePointer;
                this->privateText = text;
                this->privateSerialNumber = serialNumber;
                this->privateProductId = productId;
            }

            DeviceListItem::~DeviceListItem()
            {
                if(this->privateDevicePointer != NULL) {
                    libusb_unref_device(this->privateDevicePointer);
                }
            }

    

        int Usb::WM_DEVICECHANGEUSB() { return 0; }        

        bool Usb::supportsNotify() { return false; }

        libusb_device* Usb::notificationRegister(uint32_t guid, libusb_device *handle)
        {
            return NULL;
        }

        /// <summary>
        /// Returns a list of port names (e.g. "COM2", "COM3") for all
        /// ACM USB serial ports.  Ignores the deviceInstanceIdPrefix argument. 
        /// </param>
        /// <returns></returns>
        std::string* Usb::getPortNames(std::string deviceInstanceIdPrefix)
        {
            std::string *l = (std::string*)malloc(sizeof(std::string)*8);
            int count = 0;
            std::vector<std::string> files;
            getdir("/dev/", files);
            for (int i = 0; i < files.size(); i++) {
                if ((files[i].find("/dev/ttyACM") != std::string::npos) || (files[i].find("/dev/ttyUSB") != std::string::npos)) {
                    l[count] = files[i];
                    count++;
                }
            }
            return l;
        }

        void Usb::check()
        {
            LibUsb::handleEvents();
        }

        /*Function jacked from http://www.linuxquestions.org/questions/programming-9/c-list-files-in-directory-379323/*/
        int Usb::getdir(std::string dir, std::vector<std::string> &files)
        {
//            DIR *dp;
//            struct dirent *dirp;
//            if((dp  = opendir(dir.c_str())) == NULL) {
//                cout << "Error(" << errno << ") opening " << dir << endl;
//                return errno;
//            }
//
//            while ((dirp = readdir(dp)) != NULL) {
//                files.push_back(std::string(dirp->d_name));
//            }
//            closedir(dp);
            return 0;
        }
    
        //libusb_context LibUsb::privateContext = NULL;
        /// <summary>
        /// Raises an exception if its argument is negative, with a
        /// message describing which LIBUSB_ERROR it is.
        /// </summary>
        /// <returns>the code, if it is non-negative</returns>
        int LibUsb::throwIfError(int code)
        {
            if(code >= 0)
                return code;

            throw 1;
        }

        /// <summary>
        /// Raises an exception if its argument is negative, with a
        /// message prefixed by the message parameter and describing
        /// which LIBUSB_ERROR it is.
        /// </summary>
        /// <returns>the code, if it is non-negative</returns>
        int LibUsb::throwIfError(int code, std::string message)
        {
            try
            {
                return LibUsb::throwIfError(code);
            }
            catch (std::exception& e)
            {
                //throw LibUsb::errorDescription(code);
                std::stringstream os;
                os << LibUsb::errorDescription(code);
                throw new Exception(os.str());
            }
        }

        std::string LibUsb::errorDescription(int error)
        {
            switch(error)
            {
            case -1:
                return "I/O error.";
            case -2:
                return "Invalid parameter.";
            case -3:
                return "Access denied.";
            case -4:
                return "Device does not exist.";
            case -5:
                return "No such entity.";
            case -6:
                return "Busy.";
            case -7:
                return "Timeout.";
            case -8:
                return "Overflow.";
            case -9:
                return "Pipe error.";
            case -10:
                return "System call was interrupted.";
            case -11:
                return "Out of memory.";
            case -12:
                return "Unsupported/unimplemented operation.";
            case -99:
                return "Other error.";
            default:
                return "Unknown error code .";  //todo return error code
            };
        }

        
        libusb_context* LibUsb::privateContext = NULL;
        libusb_context* LibUsb::getContext()
        {
            if(LibUsb::privateContext == NULL)
            {
                LibUsb::throwIfError(libusb_init(&(LibUsb::privateContext)));
                libusb_set_debug(LibUsb::privateContext, 3);
            }
            return LibUsb::privateContext;
        }

        void LibUsb::handleEvents()
        {
            LibUsb::throwIfError(libusb_handle_events(LibUsb::privateContext));
        }

        /// <returns>the serial number</returns>
        std::string LibUsb::getSerialNumber(libusb_device_handle *device_handle)
        {
            libusb_device_descriptor* descriptor = getDeviceDescriptor(device_handle);
            unsigned char buffer[255];
            
            int length;
            length = LibUsb::throwIfError(libusb_get_string_descriptor_ascii(device_handle, descriptor->iSerialNumber, buffer, sizeof(buffer)), "Error getting serial number string from device");
            
            std::string serial_number = "";
            for(int i=0;i<length;i++)
            {
                serial_number += (char)buffer[i];
            }
            return serial_number;
        }

        /// <returns>true iff the vendor and product ids match the device</returns>
        bool LibUsb::deviceMatchesVendorProduct(libusb_device *device, uint16_t idVendor, uint16_t idProduct)
        {
            libusb_device_descriptor *descriptor = getDeviceDescriptorFromDevice(device);
            return idVendor == descriptor->idVendor && idProduct == descriptor->idProduct;
        }

        /// <returns>the device descriptor</returns>
        libusb_device_descriptor* LibUsb::getDeviceDescriptor(libusb_device_handle *device_handle)
        {
            return getDeviceDescriptorFromDevice(libusb_get_device(device_handle));
        }
        
        /// <returns>the device descriptor</returns>
        libusb_device_descriptor* LibUsb::getDeviceDescriptorFromDevice(libusb_device *device)
        {
            libusb_device_descriptor *descriptor = (libusb_device_descriptor*)malloc(sizeof(libusb_device_descriptor)*1);
            LibUsb::throwIfError(libusb_get_device_descriptor(device, descriptor),
                               "Failed to get device descriptor");
            return descriptor;
        }

        
 

        uint16_t UsbDevice::getProductID()
        {
            return LibUsb::getDeviceDescriptor(this->privateDeviceHandle)->idProduct;
        }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        std::string UsbDevice::getSerialNumber()
        {
            return LibUsb::getSerialNumber(this->privateDeviceHandle);
        }

        void UsbDevice::controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index)
        {
            int ret = libusb_control_transfer(this->privateDeviceHandle, RequestType, Request,
                                        Value, Index, (uint8_t*)0, 0, (uint16_t)5000);
            LibUsb::throwIfError(ret,"Control transfer failed");
        }

//        libusb_device UsbDevice::controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index, uint8_t data[], int32_t size)
//        {
//                return controlTransfer(RequestType, Request, Value, Index, data, (uint16_t)size);
//      
//        }

        uint32_t UsbDevice::controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index, unsigned char * data, uint16_t length)
        {
            int ret = libusb_control_transfer(this->privateDeviceHandle, RequestType, Request,
                                        Value, Index, data, length, (uint16_t)5000);
            LibUsb::throwIfError(ret,"Control transfer failed");
            return (uint32_t)ret;
        }


        libusb_device_handle* UsbDevice::getDeviceHandle()
        {
            return this->privateDeviceHandle;
        }

        /// <summary>
        /// Create a usb device from a deviceListItem
        /// </summary>
        /// <param name="handles"></param>
        UsbDevice::UsbDevice(DeviceListItem *deviceListItem)
        {
            LibUsb::throwIfError(libusb_open(deviceListItem->getDevicePointer(), &privateDeviceHandle),
                               "Error connecting to device.");
        }

        /// <summary>
        /// disconnects from the usb device.  This is the same as Dispose().
        /// </summary>
        void UsbDevice::disconnect()
        {
            libusb_close(this->privateDeviceHandle);
        }

        /// <summary>
        /// Disconnects from the USB device, freeing all resources
        /// that were allocated when the connection was made.
        /// This is the same as disconnect().
        /// </summary>
        void UsbDevice::Dispose()
        {
            disconnect();
        }

        /// <summary>
        /// true if the devices are the same
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool UsbDevice::isSameDeviceAs(DeviceListItem item)
        {
            return (libusb_get_device(this->privateDeviceHandle) == item.getDevicePointer());
        }

        /// <summary>
        /// gets a list of devices
        /// </summary>
        /// <returns></returns>
//        static Vector<DeviceListItem> getDeviceList(uint64_t deviceInterfaceGuid)
//        {
//            throw new NotImplementedException();
//        }

        /// <summary>
        /// gets a list of devices by vendor and product ID
        /// </summary>
        /// <returns></returns>
        std::vector<DeviceListItem*> UsbDevice::getDeviceList(uint16_t vendorId, uint16_t productIdArray[], size_t size)
        {
            std::vector<DeviceListItem*> list;

            libusb_device **device_list;
            int count = LibUsb::throwIfError(libusb_get_device_list(LibUsb::getContext(), &device_list),
                                            "Error from libusb_get_device_list.");

            int i;
            for(i=0;i<count;i++)
            {
                libusb_device *device = device_list[i];

                for(int i = 0; i < size; i++) 
                {
                    if(LibUsb::deviceMatchesVendorProduct(device, vendorId, productIdArray[i]))
                    {
                        libusb_device_handle *device_handle;
                        
                        LibUsb::throwIfError(libusb_open(device, &device_handle),
                                            "Error connecting to device to get serial number ");
//                        if(libusb_kernel_driver_active(device_handle, 0) == 1) { //find out if kernel driver is attached
//                            std::cout<<"Kernel Driver Active"<<std::endl;
//                        
//                            if(libusb_detach_kernel_driver(device_handle, 0) == 0) {//detach it
//                                std::cout<<"Kernel Driver Detached!"<<std::endl;
//                            }
//                	}
                        //libusb_clear_halt(device_handle, 0);
                        //LibUsb::throwIfError(libusb_claim_interface(device_handle, 0));
                        std::string serialNumber = LibUsb::getSerialNumber(device_handle);
                        std::ostringstream os;
                        os << "#" << serialNumber;
                        std::string serialText = os.str();
                        list.push_back(new DeviceListItem(device, serialText, serialNumber, productIdArray[i]));   //todo fix serial number string

                        libusb_close(device_handle);
                    }
                }
            }


            // Free device list without unreferencing.
            // Unreference/free the individual devices in the
            // DeviceListItem destructor.
            libusb_free_device_list(device_list, 0);

            return list;
        }

        //protected AsynchronousInTransfer newAsynchronousInTransfer(byte endpoint, uint size, uint timeout)
        //{
        //    return new AsynchronousInTransfer(this, endpoint, size, timeout);
        //}
    


//}
