#include "Exception.hpp"

/** Constructor (C strings).
 *  @param message C-style string error message.
 *                 The string contents are copied upon construction.
 *                 Hence, responsibility for deleting the \c char* lies
 *                 with the caller. 
 */
Exception::Exception(const char* message):
  msg_(message)
  {
  }

#include "Exception.hpp"
/** Constructor (C++ STL strings).
 *  @param message The error message.
 */
Exception::Exception(const std::string& message):
  msg_(message)
  {}

/** Destructor.
 * Virtual to allow for subclassing.
 */
Exception::~Exception() throw (){}

/** Returns a pointer to the (constant) error description.
 *  @return A pointer to a \c const \c char*. The underlying memory
 *          is in posession of the \c Exception object. Callers \a must
 *          not attempt to free the memory.
 */
const char* Exception::what() const throw (){
   return msg_.c_str();
}

