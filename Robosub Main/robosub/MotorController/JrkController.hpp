/* 
 * File:   JrkController.hpp
 * Author: arbiter34
 *
 * Created on February 6, 2015, 2:10 PM
 */

#ifndef JRKCONTROLLER_HPP
#define	JRKCONTROLLER_HPP

#include "protocol.h"
#include "usbdevice.hpp"
#include <sstream>

namespace Jrk 
{
            /// <summary>
        /// The different boards.
        /// </summary>
    enum jrkProduct
    {
        /// <summary>
        /// 21v3
        /// </summary>
        umc01a,
        /// <summary>
        /// 12v12
        /// </summary>
        umc02a,
        /// <summary>
        /// unknown product
        /// </summary>
        UNKNOWN
    };
    
    static std::stringstream os;
    
    class JrkController : public UsbWrapper::UsbDevice
    {
    private:
        jrkProduct product;
        static void requireArgumentRange(uint32_t argumentValue, int32_t minimum, int32_t maximum, std::string argumentName);

        uint16_t getRequest(jrkRequest requestId, uint16_t id, uint8_t length);


        /// <summary>
        /// See Sec 16.3 of the PIC18F14K50 datasheet for information about SPBRG.
        /// On the umc01a, we have SYNC=0, BRG16=1, and BRGH=1, so the pure math
        /// formula for the baud rate is Baud = INSTRUCTION_FREQUENCY / (spbrg+1);
        /// </summary>
        static uint32_t convertSpbrgToBps(uint16_t spbrg);

        /// <summary>
        /// The converts from bps to SPBRG, so it is the opposite of convertSpbrgToBps.
        /// The purse math formula is spbrg = INSTRUCTION_FREQUENCY/Baud - 1.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        static uint16_t convertBpsToSpbrg(uint32_t bps);

        static void testBaudRateConversions();

        static void assertBaudConversion(uint16_t spbrg, uint32_t bps);
		
        static void assertEqual(int32_t expected, int32_t actual, std::string description);

        uint32_t getParameter(jrkParameter parameterId);


        void setParameter(jrkParameter parameterId, uint32_t value);

        void setRequestU8(jrkRequest requestId, uint8_t id, uint8_t value);

        void setRequestU16(jrkRequest requestId, uint8_t id, uint16_t value);

    public:
        static std::string deviceInterfaceGuid;

        static std::string productName;
        static std::string shortProductName;
        
        /// <summary>
        /// The first part of the device instance ID for the bootloader for the Jrk 21v3.
        /// </summary>
        static std::string bootloaderDeviceInstanceIdPrefix82;

        /// <summary>
        /// The first part of the device instance ID for the bootloader for the next Jrk.
        /// </summary>
        static std::string bootloaderDeviceInstanceIdPrefix84;

        /// <summary>
        /// Instructions are executed at 12 MHZ
        /// </summary>
        const static int INSTRUCTION_FREQUENCY = 12000000;

        JrkController(UsbWrapper::DeviceListItem *deviceListItem);

        /// <summary>
        /// True if to get a current reading we need to divide the current variable by the duty cycle.
        /// </summary>
        bool divideCurrent();

        void setTarget(uint16_t target);

        void motorOff();

        /// <summary>
        /// Gets the jrk parameter.  The only one that is modified by this function relative to
        /// what is actually on the device is the serial baud rate.
        /// </summary>
        /// <param name="parameterId"></param>
        /// <returns></returns>
        uint32_t getJrkParameter(jrkParameter parameterId);



        /// <summary>
        /// Clears the error flag bits so that the motor can run again.
        /// Does not clear the "waiting for command" bit.
        /// </summary>
        void clearErrors();

        /// <summary>
        /// sets the parameter on the device.  The only parameter modified before setting is the
        /// serial fixed baud rate, which is converted to the PIC's SPBRG setting.
        /// </summary>
        /// <param name="parameterId"></param>
        /// <param name="value"></param>
        void setJrkParameter(jrkParameter parameterId, uint32_t value);

        void startBootloader();

        /// <summary>
        /// Reinitializes the motor controller system.
        /// </summary>
        void reinitialize();

        jrkVariables* getVariables();

        uint16_t privateFirmwareVersionMajor = 0xFFFF;
        uint8_t privateFirmwareVersionMinor = 0xFF;

        uint16_t getFirmwareVersionMajor();

        uint8_t getFirmwareVersionMinor();

        std::string getFirmwareVersionString();

        void getFirmwareVersion();

        static std::vector<UsbWrapper::DeviceListItem*> getConnectedDevices();
    };

    class Range
    {
	public:
        uint8_t uint8_ts;
        int32_t minimumValue;
        int32_t maximumValue;

        Range(uint8_t uint8_ts, int32_t minimumValue, int32_t maximumValue);

        bool isSigned();

        static Range *u16;
        static Range *u12;
        static Range *u10;
        static Range *u8;
        static Range *boolean;
    };

    class JrkExtensionMethods
    {
    public:
		static Range* range(jrkParameter parameterId);
    };
}


// Local Variables: **
// mode: java **
// c-basic-offset: 4 **
// tab-width: 4 **
// indent-tabs-mode: nil **
// end: **

#endif	/* JRKCONTROLLER_HPP */

