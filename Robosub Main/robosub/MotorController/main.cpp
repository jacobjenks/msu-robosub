#include <iostream>
#include <chrono>
#include <thread>
#include <libusb-1.0/libusb.h>
#include "JrkController.hpp"
#include "protocol.h"
#include "JrkMotor.hpp"

using namespace std;

int main() {
    //This will get the controllers in arbitrary order
    Jrk::JrkMotor *jrk = new Jrk::JrkMotor();
    
    //This will get the controllers by serialNumber
    //Jrk::JrkMotor *jrk = new Jrk::JrkMotor("39029920");
    
    if (jrk == NULL) {
        return 0;
    }
    
    cout << jrk->getSerialNumber() << endl;
    
    jrk->motorFoward(25);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorFoward(50);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));    
    
    jrk->motorFoward(75);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorFoward(50);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(25);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(50);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(75);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(100);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(75);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(50);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorBackward(25);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    
    jrk->motorStop();
}