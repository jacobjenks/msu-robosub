/* 
 * File:   JrkMotor.h
 * Author: arbiter34
 *
 * Created on February 11, 2015, 1:32 PM
 */

#ifndef JRKMOTOR_HPP
#define	JRKMOTOR_HPP

#include <stdlib.h>
#include <set>
#include <string>
#include "JrkController.hpp"
#include <memory>

enum MotorStatus{
	M_Forward,
	M_Backward,
	M_Stop,
	Error
};

//Refers to whether or not controller wires are crossed, not direction motor faces.
//Motor facing is currently handled by navigation controllers, but it wouldn't hurt to move it here.
enum MotorOrientation{
	M_Normal,
	M_Reverse
};

namespace Jrk {
    class JrkMotor {
    public:
		JrkMotor(MotorOrientation m, std::string serialNumber = "");
        JrkMotor(const JrkMotor& orig);
        virtual ~JrkMotor();
        bool motorForward(uint32_t percentPower);
        bool motorBackward(uint32_t percentPower);
        bool motorStop();
        bool controllerRestart();
        std::string getSerialNumber();
		bool controllerNull();
		std::string getStatus();
    private:
        const uint16_t POWER_DEAD_ZONE = 2047;
        const uint16_t POWER_RANGE = 2047;
        static std::set<std::string> *initializedMotors;
		static std::vector<UsbWrapper::DeviceListItem*> connectedMotors;
		bool motorPower(uint16_t power);
        JrkController *controller;
        std::string serialNumber;
        JrkController* getJrkBySerialNumber(std::string serialNumber);
        JrkController* getJrkByOrder();
		MotorStatus status;
		MotorOrientation orientation;
    };
}

#endif	/* JRKMOTOR_H */

