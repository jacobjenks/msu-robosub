/* 
 * File:   JrkMotor.cpp
 * Author: arbiter34
 * 
 * Created on February 11, 2015, 1:32 PM
 */
#include <stdlib.h>
#include <set>
#include <iostream>
#include "JrkMotor.hpp"
#include "JrkController.hpp"
#include "Exception.hpp"

using namespace Jrk;

std::set<std::string> *JrkMotor::initializedMotors = new std::set<std::string>();

std::vector<UsbWrapper::DeviceListItem*> JrkMotor::connectedMotors;

JrkMotor::JrkMotor(MotorOrientation m, std::string serialNumber) {

	if (JrkMotor::connectedMotors.empty()) {
		JrkMotor::connectedMotors = JrkController::getConnectedDevices();
	}
    //Init controller to NULL - probably not necessary
    this->controller = NULL;
    
    //If serialNumber provided, try and correct device
    if (serialNumber != "") {
        this->controller = this->getJrkBySerialNumber(serialNumber);
    } else {
        //Else get next device in order(USB order is arbitrary)
        this->controller = this->getJrkByOrder();
    }

	this->orientation = m;
	this->status = MotorStatus::M_Stop;
}

JrkMotor::JrkMotor(const JrkMotor& orig) {
}

JrkMotor::~JrkMotor() {
    //Verify the controller exists
    if (initializedMotors->find(this->serialNumber) != initializedMotors->end()) {
        //Remove the controller from the in-use list
        initializedMotors->erase(this->serialNumber);
        
        //free the controller struct from the heap
        free(this->controller);
    } 
}

std::string JrkMotor::getSerialNumber() {
    return this->serialNumber;
}

JrkController* JrkMotor::getJrkBySerialNumber(std::string serialNumber) {

    
    //Iterate over the controllers found
    for (int i = 0; i < JrkMotor::connectedMotors.size(); i++) {
        
        //If serials match
		if ((JrkMotor::connectedMotors)[i]->getSerialNumber() == serialNumber) {
            //Add to in-use list
            this->initializedMotors->insert(serialNumber);
            
            //Init and return controller
			return new JrkController((JrkMotor::connectedMotors)[i]);
        }
    }
    
    //Controller not found
    return NULL;
}

JrkController* JrkMotor::getJrkByOrder() {
    //Get list of JrkControllers connected to USB
        
    //Iterate over the controllers found
    for (int i = 0; i < JrkMotor::connectedMotors.size(); i++) {

        //Verify the controller is not already in use
		if (initializedMotors->find((JrkMotor::connectedMotors)[i]->getSerialNumber()) == initializedMotors->end()) {
            //Add to in-use list
			this->initializedMotors->insert((JrkMotor::connectedMotors)[i]->getSerialNumber());
            
            //Controller is free, initialize it and return
			return new JrkController((JrkMotor::connectedMotors)[i]);
        } 
    }

    //No controllers available
    return NULL;
}

std::string JrkMotor::getStatus(){
	switch (this->status){
		case MotorStatus::M_Forward:
			return "Forward";
			break;
		case MotorStatus::M_Backward:
			return "Backward";
			break;
		case MotorStatus::M_Stop:
			return "Stop";
			break;
		case MotorStatus::Error:
			return "Error";
			break;
		default:
			return "Unknown Error";
	}
}

bool JrkMotor::motorForward(uint32_t percentPower) {
	uint16_t rawTargetValue;
	
	if (orientation == MotorOrientation::M_Reverse)
		rawTargetValue = (uint16_t)(POWER_DEAD_ZONE + (POWER_RANGE*percentPower*.01));
	else
		rawTargetValue = (uint16_t)(POWER_DEAD_ZONE - (POWER_RANGE*percentPower*.01));
	this->status = MotorStatus::M_Forward;
	return this->motorPower(rawTargetValue);
}

bool JrkMotor::motorBackward(uint32_t percentPower) {
	uint16_t rawTargetValue;
	if (orientation == MotorOrientation::M_Reverse)
		rawTargetValue = (uint16_t)(POWER_DEAD_ZONE - (POWER_RANGE*percentPower*.01));
	else
		rawTargetValue = (uint16_t)(POWER_DEAD_ZONE + (POWER_RANGE*percentPower*.01));
	this->status = MotorStatus::M_Backward;
	return this->motorPower(rawTargetValue);
}

bool JrkMotor::motorPower(uint16_t power){
	try {
		//Set target
		this->controller->setTarget(power);

		//Test for motor controller error. Full bitwise list can be viewed at the following list, for now I just need error 2
		//https://www.pololu.com/docs/0j38/all#4.f
		std::unique_ptr<jrkVariables> vars = std::unique_ptr<jrkVariables>(this->controller->getVariables());
		if (vars->errorFlagBits == 2){
			this->status = MotorStatus::Error;
			return false;
		}

		//Verify device has new target
		return vars->target == power;
	}
	catch (Exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
}

bool JrkMotor::motorStop() {
    try {
        //Send motorOff command
        this->controller->motorOff();

		std::unique_ptr<jrkVariables> vars = std::unique_ptr<jrkVariables>(this->controller->getVariables());
		if (vars->errorFlagBits == 2){
			this->status = MotorStatus::Error;
			return false;
		}

		this->status = MotorStatus::M_Stop;
        
        //Verify command took, check Awaiting Command bit is flipped
        return vars->errorFlagBits & 1;
    } catch (Exception& e) {
        //Communication error
        std::cout << e.what() << std::endl;
        //Report failure
        return false;
    }
}

bool JrkMotor::controllerRestart() {
    try {
        //Send reinit
        this->controller->reinitialize();
        
        //Verify we can talk to controller after reinit
        return this->controller->getSerialNumber() == this->serialNumber;
    } catch (Exception& e) {
        //Communication error
        std::cout << e.what() << std::endl;
        //Report failure
        return false;
    }
}

bool JrkMotor::controllerNull(){
	if (controller == NULL)
		return true;
	else
		return false;
}