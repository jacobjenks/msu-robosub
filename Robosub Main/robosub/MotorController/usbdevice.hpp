/* 
 * File:   usbdevice.hpp
 * Author: arbiter34
 *
 * Created on February 4, 2015, 7:17 PM
 */

#ifndef USBDEVICE_HPP
#define	USBDEVICE_HPP

#include <libusb-1.0/libusb.h>
#include <string>
#include <vector>
#include <exception>
#include <stdlib.h>
#include <stdio.h>

namespace UsbWrapper {
    
    /// <summary>
    /// A class that represents a device connected to the computer.  This
    /// class can be used as an item in the device list dropdown box.
    /// </summary>
    class DeviceListItem
    {
        private:
            std::string privateText, privateSerialNumber;

            libusb_device *privateDevicePointer;

            uint16_t privateProductId;

        /// <summary>
        /// The text to display to the user in the list to represent this
        /// device.  By default, this text is "#" + serialNumberString,
        /// but it can be changed to suit the application's needs
        /// (for example, adding model information to it).
        /// </summary>

        public:
        
            /// <summary>
            /// The text to display to the user in the list to represent this
            /// device.  By default, this text is "#" + serialNumberString,
            /// but it can be changed to suit the application's needs
            /// (for example, adding model information to it).
            /// </summary>
            std::string getText();

            void setText(std::string text);


            /// <summary>
            /// Gets the serial number.
            /// </summary>
            std::string getSerialNumber();

            /// <summary>
            /// Gets the USB product ID of the device.
            /// </summary>
            uint16_t getProductID();

            /// <summary>
            /// true if the devices are the same
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            bool isSameDeviceAs(DeviceListItem item);



            /// <summary>
            /// Creates an item that doesn't actually refer to a device; just for populating the list with things like "Disconnected"
            /// </summary>
            /// <param name="text"></param>
            static DeviceListItem CreateDummyItem(std::string text);

            /// <summary>
            /// Gets the device pointer.
            /// </summary>
            libusb_device* getDevicePointer();



            DeviceListItem(libusb_device *devicePointer, std::string text, std::string serialNumber, uint16_t productId);

            ~DeviceListItem();
    };
    

    class Usb
    {
    public: 
        static int WM_DEVICECHANGEUSB();       

        static bool supportsNotify();

        static libusb_device* notificationRegister(uint32_t guid, libusb_device *handle);

        /// <summary>
        /// Returns a list of port names (e.g. "COM2", "COM3") for all
        /// ACM USB serial ports.  Ignores the deviceInstanceIdPrefix argument. 
        /// </param>
        /// <returns></returns>
        static std::string* getPortNames(std::string deviceInstanceIdPrefix);

        static void check();

        /*Function jacked from http://www.linuxquestions.org/questions/programming-9/c-list-files-in-directory-379323/*/
        static int getdir(std::string dir, std::vector<std::string> &files);
    };

    class LibUsb
    {
    private:
        /// <summary>
        /// Do not use directly.  The property below initializes this
        /// with libusb_init when it is first used.
        /// </summary>
        static libusb_context *privateContext;
            
    public:
        
        static void init() {
            
        }
        /// <summary>
        /// Raises an exception if its argument is negative, with a
        /// message describing which LIBUSB_ERROR it is.
        /// </summary>
        /// <returns>the code, if it is non-negative</returns>
        static int throwIfError(int code);

        /// <summary>
        /// Raises an exception if its argument is negative, with a
        /// message prefixed by the message parameter and describing
        /// which LIBUSB_ERROR it is.
        /// </summary>
        /// <returns>the code, if it is non-negative</returns>
        static int throwIfError(int code, std::string message);

        static std::string errorDescription(int error);

        

        static libusb_context* getContext();

        static void handleEvents();

        /// <returns>the serial number</returns>
        static std::string getSerialNumber(libusb_device_handle *device_handle);

        /// <returns>true iff the vendor and product ids match the device</returns>
        static bool deviceMatchesVendorProduct(libusb_device *device, uint16_t idVendor, uint16_t idProduct);

        /// <returns>the device descriptor</returns>
        static libusb_device_descriptor* getDeviceDescriptor(libusb_device_handle *device_handle);
        
        /// <returns>the device descriptor</returns>
        static libusb_device_descriptor* getDeviceDescriptorFromDevice(libusb_device *device);
        
    };

    class UsbDevice
    {
    private:        
        libusb_device_handle *privateDeviceHandle;
        
    public:
        uint16_t getProductID();

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        std::string getSerialNumber();

        void controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index);

//        libusb_device* controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index, unsigned char *data);

        uint32_t controlTransfer(uint8_t RequestType, uint8_t Request, uint16_t Value, uint16_t Index, unsigned char *data, uint16_t length);


        libusb_device_handle* getDeviceHandle();

        /// <summary>
        /// Create a usb device from a deviceListItem
        /// </summary>
        /// <param name="handles"></param>
        UsbDevice(DeviceListItem *deviceListItem);

        /// <summary>
        /// disconnects from the usb device.  This is the same as Dispose().
        /// </summary>
        void disconnect();

        /// <summary>
        /// Disconnects from the USB device, freeing all resources
        /// that were allocated when the connection was made.
        /// This is the same as disconnect().
        /// </summary>
        void Dispose();

        /// <summary>
        /// true if the devices are the same
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool isSameDeviceAs(DeviceListItem item);

        /// <summary>
        /// gets a list of devices
        /// </summary>
        /// <returns></returns>
//        static Vector<DeviceListItem> getDeviceList(uint64_t deviceInterfaceGuid)
//        {
//            throw new NotImplementedException();
//        }

        /// <summary>
        /// gets a list of devices by vendor and product ID
        /// </summary>
        /// <returns></returns>
        static std::vector<DeviceListItem*> getDeviceList(uint16_t vendorId, uint16_t *productIdArray, size_t size);

        //protected AsynchronousInTransfer newAsynchronousInTransfer(byte endpoint, uint size, uint timeout)
        //{
        //    return new AsynchronousInTransfer(this, endpoint, size, timeout);
        //}
    };



}

#endif	/* USBDEVICE_HPP */

