/* 
 * File:   JrkController.hpp
 * Author: arbiter34
 *
 * Created on February 6, 2015, 2:10 PM
 */
#include <exception>
#include <string>

#ifndef EXCEPTION_HPP
#define	EXCEPTION_HPP

class Exception : public std::exception
{
public:
    /** Constructor (C strings).
     *  @param message C-style string error message.
     *                 The string contents are copied upon construction.
     *                 Hence, responsibility for deleting the \c char* lies
     *                 with the caller. 
     */
    explicit Exception(const char* message);

    /** Constructor (C++ STL strings).
     *  @param message The error message.
     */
    explicit Exception(const std::string& message);

    /** Destructor.
     * Virtual to allow for subclassing.
     */
    virtual ~Exception() throw ();

    /** Returns a pointer to the (constant) error description.
     *  @return A pointer to a \c const \c char*. The underlying memory
     *          is in posession of the \c Exception object. Callers \a must
     *          not attempt to free the memory.
     */
    virtual const char* what() const throw ();

protected:
    /** Error message.
     */
    std::string msg_;
};

// Local Variables: **
// mode: java **
// c-basic-offset: 4 **
// tab-width: 4 **
// indent-tabs-mode: nil **
// end: **

#endif	/* EXCEPTION_HPP */