# RoboSub Code Setup Guide
This is the repository for the RoboSub Club at Montana State University. The RoboSub code is written in C++, and uses OpenCV for vision processing.

## Software you need
* Visual Studio 2013
* OpenCV
* Git

## Visual Studio 2013
Visual studio is the IDE we picked to use on this project. You're more than welcome to use any development environment you wish, but using Visual Studio might make your life easier. You can download install Visual Studio 2013 [here](https://www.visualstudio.com/en-us/post-download-vs?sku=community&clcid=0x409).


## OpenCV and Vision Processing
OpenCV is the open source vision software we use on the project. Vision processing is a critical component of the project, since nearly all of the tasks require visual identification. The current vision processing pipeline for finding a target looks something like this:

* Convert image from camera to HSV
* Use color thresholding to filter out everything but the competition target
* Use edge detection to find edges of target
* Use contour detection to turn edges into a shape
* Draw a minimum bounding circle around the largest contour in the image
* Return the center of the circle

The center of the target is then passed to the navigation component of the software, which decides how to move towards the target.

You can download OpenCV version 2.4.10 [here](http://opencv.org/downloads.html). I believe OpenCV downloads as an executable that extracts the library to a file directory of your choice. You can keep the library anywhere you like. I like to keep mine in C:\opencv.

## Git
We use Git for version control. Version control is a method software developers use to work collaboratively on a project. It allows them to all work on the code at once without the risk of anyone overwriting anyone else's changes. Bitbucket is a free Git project hosting service that allows us to keep our code on the internet where anyone can access it, which makes collaboration much easier.

You can download Git [here](https://git-scm.com/). Git also has [excellent documentation](https://git-scm.com/doc) that can teach you everything you need to know about it - I would highly recommend running through the basics.

Once you have Git installed you should download the repo. To do that open up the Git Bash program you just installed, and type: git clone https://bitbucket.org/jacobjenks/msu-robosub.git C:\path\to\dir. C:\path\to\dir should be the directory you'd like to keep the repo in.

## Final steps
The last thing you need to do is set up some environmental variables to tell your computer where the OpenCV libraries you downloaded are. I'm afraid I have only done this on Windows, so if you're using another operating system this setup will be a little different. Here are the step-by-step instructions for Windows. In this guide I enclose all values in single quotes - do not include these quotes when typing them in to the relevant fields.

* Right click on 'My Computer' and click properties.
* Click on 'Advanced system settings'
* Click on 'Environmental Variables'
* Under the 'System variables' section click 'New...'
* Use 'OPENCV_DIR' as the variable name, and then the path of the \build directory within your OpenCV folder as the variable value. Since I put my OpenCV folder in C, my path is 'C:\opencv\build'
* Find the 'Path' variable in your 'System variables' section, and click 'Edit...'
* Be very careful while doing this step! Take special care to not erase any of the existing text within your Path, since this can do bad things to your computer.
* Scroll to the end of the text within the 'Variable value' field, and add ';%OPENCV_DIR%\x86\vc12\bin;%OPENCV_DIR%\x86\vc12\lib;%OPENCV_DIR%\x86\vc12\staticlib;'
* Click 'OK', and you should be ready to run the code!

Now you can open Visual Studio, and then open the Visual Studio project file within the repo you downloaded earlier. If all goes well, you should now be able to run the RoboSub code!
		